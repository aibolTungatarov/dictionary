//
//  DynamicHeightTableView.swift
//  FDUIKit
//
//  Created by iBol on 8/10/21.
//

import Foundation

public class SelfSizingTableView: UITableView {
	override public var contentSize: CGSize {
		didSet {
			invalidateIntrinsicContentSize()
			setNeedsLayout()
		}
	}

	override public var intrinsicContentSize: CGSize {
		let height = min(.infinity, contentSize.height)
		return CGSize(width: contentSize.width, height: height)
	}
}
