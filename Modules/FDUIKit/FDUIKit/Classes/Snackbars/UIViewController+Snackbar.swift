//
//  UIViewController+Snackbar.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import Foundation
import Toast_Swift
import UIKit

//private enum Constants {
//	static let defaultSnackbarDuration: TimeInterval = 2
//}
//
//public extension UIViewController {
//	func showSuccess(message: String?) {
//		view.makeToast(message, position: .top)
//	}
//	
//	func showError(message: String?) {
//		var style = ToastStyle()
//		style.messageColor = .red
//		view.makeToast(message, position: .top)
//	}
//}
