//
//  FloatingTextField.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 26.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation
import SkyFloatingLabelTextField

@objc
public class FloatingTextField: SkyFloatingLabelTextField {
    private var padding: UIEdgeInsets?
    
    private var defaultInsets = UIEdgeInsets(top: 0, left: 0, bottom: -12, right: 0)
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        lineColor = UIColor.silver
        selectedLineColor = UIColor.lightningYellow
        selectedTitleColor = UIColor.lightningYellow
        titleColor = UIColor(hex: "999999")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        lineColor = UIColor.silver
        selectedLineColor = UIColor.lightningYellow
        selectedTitleColor = UIColor.lightningYellow
        titleColor = UIColor(hex: "999999")
    }
    
	public func setPadding(_ padding: UIEdgeInsets) {
        self.padding = padding
        
        setNeedsDisplay()
    }
    
    override public func textRect(forBounds bounds: CGRect) -> CGRect {
        guard let padding = padding else {
            return bounds.inset(by: defaultInsets)
        }
        
        return bounds.inset(by: padding)
    }
    
    override public func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        guard let padding = padding else {
            return bounds.inset(by: defaultInsets)
        }
        
        return bounds.inset(by: padding)
    }
    
    override public func editingRect(forBounds bounds: CGRect) -> CGRect {
        guard let padding = padding else {
            return bounds.inset(by: defaultInsets)
        }

        return bounds.inset(by: padding)
    }
}
