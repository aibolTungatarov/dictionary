//
//  CustomSegmentedControl.swift
//  FDUIKit
//
//  Created by iBol on 6/8/21.
//

import SnapKit

public final class CustomSegmentedControl: UISegmentedControl {
	private let segmentIndicator = UIView()
	///Находит centerX point для того, чтобы проставить segment indicator в центр выбранного сегмента
	private var divider: CGFloat {
		return CGFloat(numberOfSegments) / CGFloat(3 + (selectedSegmentIndex - 1) * 2)
	}
	
	public override init(items: [Any]?) {
		super.init(items: items)
		setupView()
		addTarget(self, action: #selector(valueChanged(_:)), for: .valueChanged)
	}
	
	@available(*, unavailable, message: "Init via init(items)")
	public override init(frame: CGRect) {
		super.init(frame: frame)
	}
	
	@available(*, unavailable, message: "Init via init(items)")
	public init() {
		super.init(frame: .zero)
	}
	
	required init?(coder: NSCoder) {
		nil
	}
	
	private func setupView() {
		tintColor = .clear
		backgroundColor = .clear
		clipsToBounds = false
		setBackgroundImage(UIImage(), for: .normal, barMetrics: .default)
		setDividerImage(UIImage(), forLeftSegmentState: .normal, rightSegmentState: .normal, barMetrics: .default)
		if #available(iOS 13.0, *) {
			selectedSegmentTintColor = .clear
		}
		[segmentIndicator].forEach(addSubview(_:))
		segmentIndicator.backgroundColor = .appOrange
		setTitleTextAttributes(Constants.defaultTextAttributes, for: .normal)
		setupConstraints()
	}
	
	private func setupConstraints() {
		segmentIndicator.snp.makeConstraints { make in
			make.height.equalTo(4)
			make.bottom.equalToSuperview()
			make.width.equalToSuperview().dividedBy(numberOfSegments)
			make.centerX.equalToSuperview().dividedBy(numberOfSegments)
		}
	}
	
	public func updateBottomConstraint() {
		segmentIndicator.snp.remakeConstraints { make in
			make.height.equalTo(Constants.segmentIndicatorHeight)
			make.bottom.equalToSuperview()
			make.width.equalToSuperview().dividedBy(numberOfSegments)
			make.centerX.equalToSuperview().dividedBy(divider)
		}
		UIView.animate(withDuration: 0.4,
					   animations: {
						self.layoutIfNeeded()
						self.segmentIndicator.transform = Constants.segmentIndicatorChangeTransform
					   },
					   completion: { _ in
						self.resetSegmentIndicatorTransform()
					   })
	}
	
	private func resetSegmentIndicatorTransform() {
		UIView.animate(withDuration: 0.3) {
			self.segmentIndicator.transform = CGAffineTransform.identity
		}
	}
	
	@objc
	private func valueChanged(_ control: UISegmentedControl) {
		updateBottomConstraint()
	}
}

private enum Constants {
	static let defaultTextAttributes: [NSAttributedString.Key: Any] = [
		.foregroundColor: UIColor.black,
		.font: FontFamily.Roboto.regular.font(size: 14),
		.backgroundColor: UIColor.clear
	]
	static let segmentIndicatorHeight: CGFloat = 4
	static let bottomLineBackgroundColor = UIColor.white.withAlphaComponent(0.47)
	static let segmentIndicatorChangeTransform = CGAffineTransform(scaleX: 0.6, y: 1)
}
