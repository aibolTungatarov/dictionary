//
//  CustomPickerView.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 20.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public class CustomPickerView: UIPickerView {
    
    // MARK: - Inits
    public init(delegate: (UIPickerViewDelegate & UIPickerViewDataSource)?) {
        super.init(frame: .zero)
        configure(with: delegate)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Methods
	private func configure(with delegate: (UIPickerViewDelegate & UIPickerViewDataSource)?) {
		self.dataSource = delegate
		self.delegate = delegate
	}
}
