//
//  UILabelFactory.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 16.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public final class UILabelFactory {
    private let label: UILabel
    private let defultFontSize: CGFloat = 20

    public enum Style {
       case normal
     }

    // MARK: - Inits
    public init(text: String?, style: Style = .normal) {
        label = UILabel()
        label.text = text
        label.font = label.font.withSize(defultFontSize)

        switch style {
        case .normal:
            normalStyle()
        }
    }


    // MARK: - Styles
    public func normalStyle() {
        label.numberOfLines = 0
    }

    // MARK: - Public methods
    public func font(size: CGFloat) -> Self {
        label.font = label.font.withSize(size)
            
        return self
    }

    public func text(color: UIColor) -> Self {
        label.textColor = color
            
        return self
    }

    public func font(_ font: UIFont) -> Self {
        label.font = font
        
        return self
    }

    public func text(alignment: NSTextAlignment) -> Self {
        label.textAlignment = alignment
        
        return self
    }

    public func numberOf(lines: Int) -> Self {
        label.numberOfLines = lines
            
        return self
    }
    
    public func isHidden(_ hidden: Bool) -> Self {
        label.isHidden = hidden
        
        return self
    }
    
    public func adjustsFontSizeToFitWidth(_ flag: Bool) -> Self {
        label.adjustsFontSizeToFitWidth = flag
        
        return self
    }
	
	public func backgroundColor(_ color: UIColor) -> Self {
		label.backgroundColor = color
		return self
	}

    public func build() -> UILabel {
        return label
    }
}
