//
//  UICollectionViewFactory.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 19.12.2020.
//  Copyright © 2020 chocolife.me. All rights reserved.
//

import UIKit

public final class UICollectionViewFactory {
    
    private let collectionView: UICollectionView
    
    public init(layout: UICollectionViewLayout) {
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    }
    
    public func register<T: UICollectionViewCell>(_ aClass: T.Type) -> Self {
		collectionView.register(aClass, forCellWithReuseIdentifier: String(describing: aClass))
        
        return self
    }
    
    public func allowsMultipleSelection(_ flag: Bool) -> Self {
        collectionView.allowsMultipleSelection = true
        
        return self
    }
    
    public func showsScrollIndicators(vertical: Bool = true, horizontal: Bool = true) -> Self {
        collectionView.showsVerticalScrollIndicator = vertical
        collectionView.showsHorizontalScrollIndicator = horizontal
        
        return self
    }
    
    public func isScrollEnabled(_ flag: Bool) -> Self {
        collectionView.isScrollEnabled = flag
        
        return self
    }
    
    public func contentInset(_ insets: UIEdgeInsets) -> Self {
        collectionView.contentInset = insets
        
        return self
    }
    
    public func backgroundColor(_ color: UIColor) -> Self {
        collectionView.backgroundColor = color
        
        return self
    }
    
    public func build() -> UICollectionView {
        return collectionView
    }
}
