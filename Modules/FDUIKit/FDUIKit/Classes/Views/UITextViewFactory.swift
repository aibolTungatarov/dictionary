//
//  UITextViewFactory.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 20.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit
import SkeletonView

public final class UITextViewFactory {
    
    private let _textView: TextView
    private let _defaultFontSize: CGFloat = 14
    
	public enum UITextViewType {
        case `default`
        case growing
    }

    convenience public init(placeholder: String? = nil, padding: UIEdgeInsets? = nil) {
        self.init(type: .default, placeholder: placeholder)
        
        guard let padding = padding else { return }
        _textView.textContainerInset = padding
    }
    
    public init(type: UITextViewType, placeholder: String? = nil) {
        switch type {
        case .default:
            _textView = TextView()
        case .growing:
            _textView = GrowingTextView()
        }
        _textView.backgroundColor = .clear
        _textView.font = FontFamily.Roboto.regular.font(size: _defaultFontSize)
        _textView.textColor = .appBlack
        _textView.isScrollEnabled = false
        _textView.placeholder = placeholder
        _textView.placeholderColor = .interfaceGray
    }
    
    public func background(color: UIColor) -> Self {
        _textView.backgroundColor = color
        
        return self
    }
    
    public func isEditable(_ flag: Bool) -> Self {
        _textView.isEditable = flag
        
        return self
    }
    
    public func isSelectable(_ flag: Bool) -> Self {
        _textView.isSelectable = flag
        
        return self
    }
    
    public func isScrollingEnabled(_ flag: Bool) -> Self {
        _textView.isScrollEnabled = flag
        
        return self
    }
    
    public func placeholder(_ placeholder: String?) -> Self {
        _textView.placeholder = placeholder
        
        return self
    }
    
    public func placeholder(color: UIColor) -> Self {
        _textView.placeholderColor = color
        
        return self
    }
    
    public func text(color: UIColor) -> Self {
        _textView.textColor = color
        
        return self
    }
    
    public func font(_ font: UIFont) -> Self {
        _textView.font = font
        
        return self
    }
    
    public func font(size: CGFloat) -> Self {
        _textView.font = _textView.font.withSize(CGFloat(size))
        
        return self
    }
    
    public func text(alignment: NSTextAlignment) -> Self {
        _textView.textAlignment = alignment
        
        return self
    }
    
    public func content(inset: UIEdgeInsets) -> Self {
        _textView.contentInset = inset
        
        return self
    }
    
    public func textContainer(inset: UIEdgeInsets) -> Self {
        _textView.textContainerInset = inset
        
        return self
    }
    
    public func border(width: CGFloat = 1, color: UIColor) -> Self {
        _textView.layer.borderColor = color.cgColor
        _textView.layer.borderWidth = width
        
        return self
    }
    
    public func corner(radius: CGFloat) -> Self {
        _textView.corner(radius: radius)
        
        return self
    }
    
    public func minGrowing(height: CGFloat) -> Self {
        guard let growingTextView = _textView as? GrowingTextView else {
            return self
        }
        growingTextView.minHeight = height
        
        return self
    }
    
    public func isSkeletonable(_ flag: Bool, cornerRadius: Int = 4) -> Self {
        _textView.isSkeletonable = flag
        _textView.linesCornerRadius = cornerRadius
        
        return self
    }
    
    public func build() -> TextView {
        return _textView
    }
}
