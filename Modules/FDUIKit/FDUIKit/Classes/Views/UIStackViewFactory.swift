//
//  UIStackViewFactory.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 03.11.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

final class UIStackViewFactory {
    private let _stackView: UIStackView
    
    init(axis: NSLayoutConstraint.Axis) {
        _stackView = UIStackView()
        _stackView.axis = axis
        _stackView.alignment = .fill
    }
    
    public func spacing(_ value: CGFloat) -> Self {
        _stackView.spacing = value
        
        return self
    }
    
    public func distribution(_ value: UIStackView.Distribution) -> Self {
        _stackView.distribution = value
        
        return self
    }
    
    public func build() -> UIStackView {
        return _stackView
    }
}

