//
//  EmptyView.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 14.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public class EmptyView: UIView {
    // MARK: - Views
    private let emptyLabel =
        UILabelFactory(text: "")
            .text(color: .interfaceGray)
            .font(FontFamily.Roboto.regular.font(size: 14))
            .build()
    
    // MARK: - Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubviews()
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func set(text: String, textColor: UIColor = .interfaceGray) {
        emptyLabel.text = text
        emptyLabel.textColor = textColor
    }
}

extension EmptyView: Decoratable {
	public func addSubviews() {
        addSubviews([
            emptyLabel
        ])
    }
    
	public func setupLayout() {
        emptyLabel.snp.makeConstraints { (make) in
            make.top.equalTo(safeAreaLayoutGuide).offset(24)
            make.centerX.equalToSuperview()
        }
    }
    
    
}
