//
//  UITextfieldFactory.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 16.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public final class UITextFieldFactory {	
	private let textField: RegularTextField
	
	public enum Style {
		case normal
		case defaultTextField
	}
	
	private let optionalLabel: UILabel =
		UILabelFactory(text: "*")
		.text(color: .appOrange)
		.font(size: 18)
		.build()
	
	private var isOptional: Bool = false
	// MARK: - Inits
	public init(placeholder: String? = nil, style: Style = .defaultTextField, padding: UIEdgeInsets = .zero, textField: RegularTextField = .init(), isOptional: Bool = false) {
		self.textField = textField
		optionalLabel.isHidden = !isOptional
		textField.placeholder = placeholder
		
		switch style {
		case .normal:
			normalStyle()
		case .defaultTextField:
			defaultStyle()
		}
		setupViews()
		setupConstraints()
	}
	
	private func setupViews() {
		textField.addSubview(optionalLabel)
	}
	
	private func setupConstraints() {
		optionalLabel.snp.makeConstraints { make in
			make.leading.equalToSuperview()
			make.top.equalTo(textField).offset(-3)
		}
	}
	
	// MARK: - Styles
	public func normalStyle() {
		textField.autocorrectionType = .no
		textField.autocapitalizationType = .none
	}
	
	public func defaultStyle() {
		textField.backgroundColor = .white
	}
	
	// MARK: - Public methods
	public func corner(radius: CGFloat) -> Self {
		textField.layer.cornerRadius = radius
		
		return self
	}
	
	public func border(style: UITextField.BorderStyle) -> Self {
		textField.borderStyle = style
		textField.layer.borderWidth = 0
		
		return self
	}
	
	public func keyboard(Type: UIKeyboardType) -> Self {
		textField.keyboardType = Type
		
		return self
	}
	
	public func font(size: CGFloat, minimumSize: CGFloat = 14) -> Self {
		textField.font = textField.font?.withSize(size)
		textField.minimumFontSize = minimumSize
		
		return self
	}
	
	public func text(color: UIColor) -> Self {
		textField.textColor = color
		
		return self
	}
	
	public func text(contentType: UITextContentType) -> Self {
		if #available(iOS 12.0, *) {
			textField.textContentType = contentType
		}
		
		return self
	}
	
	public func font(_ font: UIFont) -> Self {
		textField.font = font
		
		return self
	}
	
	public func tint(color: UIColor) -> Self {
		textField.tintColor = color
		
		return self
	}
	
	public func build() -> RegularTextField {
		return textField
	}
	
	public func setButtonImage(image: UIImage?, resignImage: UIImage? = nil, tintColor: UIColor = .lightGray) -> Self {
		guard let textField = textField as? RightButtonTextField else { return self }
		textField.setButtonImage(image: image, resignImage: resignImage)
		textField.tintColor = tintColor
		return self
	}
	
	public func setLeftImage(image: UIImage?, tintColor: UIColor = .appOrange) -> Self {
		textField.setLeftImage(image: image, tintColor: tintColor)
		return self
	}
	
	public func isOptional(_ value: Bool) -> Self {
		optionalLabel.isHidden = !value
		return self
	}
}
