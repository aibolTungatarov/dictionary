//
//  RequestSuccessAlertView.swift
//  FDUIKit
//
//  Created by iBol on 6/8/21.
//

import UIKit

public protocol RequestSuccessAlertViewDelegate: AnyObject {
	func didTapOkButton()
}

public class RequestSuccessAlertView: UIView {
	public weak var delegate: RequestSuccessAlertViewDelegate?
	public var didTapOkButtonClosure: (() -> Void)?
	
	// MARK: - Views Factory
	private var logoImageView = UIImageView(image: .init(imageLiteralResourceName: "success-checkmark"))
	private let titleLabel =
		UILabelFactory(text: "Уважаемый клиент!")
		.font(FontFamily.Roboto.bold.font(size: 18))
		.text(alignment: .center)
		.text(color: .appBlack)
		.build()
	
	private let subtitleLabel =
		UILabelFactory(text: "Ваша заявка успешна отправлена")
		.font(FontFamily.Roboto.bold.font(size: 14))
		.text(alignment: .center)
		.text(color: .appGray)
		.build()
	
	private let okButton: UIButton = {
		let view = UIButton.primaryUnderDevButton(title: "Хорошо")
		let shadowOffset = CGSize(width: 0, height: 4)
		view.addShadow(ofColor: .white, radius: 10, offset: shadowOffset, opacity: 0.1)
		view.tap(target: self, selector: #selector(didTapOkButton))
		return view
	}()
	
	private let alertView: UIView = {
		let view = UIView()
		view.corner(radius: 20)
		view.backgroundColor = .white
		return view
	}()
	
	public init() {
		super.init(frame: .zero)
		
		backgroundColor = .white
		addSubviews()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	@objc func didTapOkButton() {
		didTapOkButtonClosure?()
	}
}

extension RequestSuccessAlertView: Decoratable {
	public func addSubviews() {
		[alertView].forEach { addSubview($0) }
		[logoImageView, titleLabel, subtitleLabel, okButton].forEach { alertView.addSubview($0) }
	}
	
	public func setupLayout() {
		alertView.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(30)
			make.right.equalToSuperview().offset(-30)
			make.center.equalToSuperview()
		}
		
		logoImageView.snp.makeConstraints { make in
			make.width.height.equalTo(100)
			make.top.equalToSuperview().offset(80)
			make.centerX.equalToSuperview()
		}
		
		titleLabel.snp.makeConstraints { make in
			make.top.equalTo(logoImageView.snp.bottom).offset(25)
			make.left.equalToSuperview().offset(30)
			make.right.equalToSuperview().offset(-30)
			make.center.equalToSuperview()
		}
		
		subtitleLabel.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(20)
			make.right.equalToSuperview().offset(-20)
			make.top.equalTo(titleLabel.snp.bottom).offset(10)
		}
		
		okButton.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(20)
			make.right.equalToSuperview().offset(-20)
			make.top.equalTo(subtitleLabel.snp.bottom).offset(30)
			make.bottom.equalToSuperview().offset(-30)
			make.height.equalTo(50)
		}
	}
}
