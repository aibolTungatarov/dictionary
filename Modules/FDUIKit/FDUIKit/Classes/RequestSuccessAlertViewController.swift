//
//  RequestSuccessAlertViewController.swift
//  FDUIKit
//
//  Created by iBol on 6/8/21.
//

import UIKit

public class RequestSuccessAlertViewController: UIViewController {
	public init() {
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public override func loadView() {
		let alertView = RequestSuccessAlertView()
		alertView.delegate = self
		view = alertView
	}
}

extension RequestSuccessAlertViewController: RequestSuccessAlertViewDelegate {
	public func didTapOkButton() {
		dismiss(animated: true)
	}
}
