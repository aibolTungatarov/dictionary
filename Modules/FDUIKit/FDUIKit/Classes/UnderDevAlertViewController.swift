//
//  UnderDevAlertViewController.swift
//  FDUIKit
//
//  Created by iBol on 5/13/21.
//

import UIKit

public class UnderDevAlertViewController: UIViewController {
	public init() {
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public override func loadView() {
		let alertView = UnderDevAlertView()
		alertView.delegate = self
		view = alertView
	}
}

extension UnderDevAlertViewController: UnderDevAlertViewDelegate {
	public func didTapOkButton() {
		dismiss(animated: true)
	}
}
