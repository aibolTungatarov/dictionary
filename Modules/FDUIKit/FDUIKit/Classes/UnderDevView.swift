//
//  UnderDevView.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 5/12/21.
//

import UIKit

public class UnderDevView: UIView {
	// MARK: - Views Factory
	private let titleLabel =
		UILabelFactory(text: "Согласно расчету...")
		.font(FontFamily.Roboto.bold.font(size: 18))
		.text(alignment: .center)
		.text(color: .appBlack)
		.build()
	
	private let subtitleLabel =
		UILabelFactory(text: "Эта страница сейчас в разработке")
		.font(FontFamily.Roboto.bold.font(size: 14))
		.text(alignment: .center)
		.text(color: .appGray)
		.build()
	
	public init() {
		super.init(frame: .zero)
		
		backgroundColor = .white
		addSubviews()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension UnderDevView: Decoratable {
	public func addSubviews() {
		[titleLabel, subtitleLabel].forEach { addSubview($0) }
	}
	
	public func setupLayout() {
		titleLabel.snp.makeConstraints { make in
			make.centerX.equalToSuperview()
		}
		
		subtitleLabel.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(20)
			make.right.equalToSuperview().offset(-20)
			make.top.equalTo(titleLabel.snp.bottom).offset(10)
		}
	}
}
