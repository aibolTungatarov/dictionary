//
//  TableView.swift
//  FDUIKit
//
//  Created by iBol on 5/12/21.
//

import UIKit

public protocol TableLoadMoreManagerDataSource: class {
	var loadMoreControl: LoadMoreManager? { get }
}

public enum RefreshState: Int {
	case refresh
	case loadMore
}

public protocol LoadMoreRefreshManagerDelegate: class {
	func loadMore()
}

public protocol RefreshManagerDelegate: class {
	func refresh()
}

public protocol LoadMoreAnimationDelegate: class {
	func startAnimating()
}

public class TableView: UITableView {
	
	public var totalNumberOfRows: Int {
		var totalNumber = 0
		for section in 0 ..< numberOfSections {
			for _ in 0 ..< numberOfRows(inSection: section) {
				totalNumber += 1
			}
		}
		
		return totalNumber
	}
	
	public lazy var topRefreshController: UIRefreshControl = {
		let refreshControl = UIRefreshControl()
		refreshControl.addTarget(self, action: #selector(refreshTableView(_:)), for: .valueChanged)
		
		return refreshControl
	}()
	
	public lazy var bottomView: UIView = {
		let infiniteScrollingView = UIView(frame: CGRect(x: 0, y: self.contentSize.height, width: self.bounds.size.width, height: 60))
		infiniteScrollingView.autoresizingMask = UIView.AutoresizingMask.flexibleWidth
		let activityViewIndicator = UIActivityIndicatorView(style: .gray)
		activityViewIndicator.frame = CGRect(x: infiniteScrollingView.frame.size.width / 2 - activityViewIndicator.frame.width / 2, y: infiniteScrollingView.frame.size.height / 2 - activityViewIndicator.frame.height / 2, width: activityViewIndicator.frame.width, height: activityViewIndicator.frame.height)
		activityViewIndicator.startAnimating()
		infiniteScrollingView.addSubview(activityViewIndicator)
		
		return infiniteScrollingView
	}()
	
	public lazy var loadMoreManager: LoadMoreManager = {
		let manager = LoadMoreManager()
		return manager
	}()
	
	public weak var loadMoreRefreshManagerDelegate: LoadMoreRefreshManagerDelegate? {
		didSet {
			if let _ = self.loadMoreRefreshManagerDelegate {
				self.loadMoreManager.delegate = self
			} else {
				self.loadMoreManager.delegate = nil
			}
		}
	}
	
	public weak var refreshManagerDelegate: RefreshManagerDelegate? {
		didSet {
			if let _ = self.refreshManagerDelegate {
				self.refreshControl = self.topRefreshController
			} else {
				self.refreshControl = nil
			}
		}
	}
	
	public var tableViewContentSize: CGSize {
		layoutIfNeeded()

		return contentSize
	}
	
	fileprivate func settings() {
		showsVerticalScrollIndicator = false
		separatorStyle = .none
		estimatedRowHeight = 44
		rowHeight = UITableView.automaticDimension
	}
	
	public override init(frame: CGRect, style: UITableView.Style) {
		super.init(frame: frame, style: style)
		settings()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	@objc public func endRefreshingTable(loadMoreStatus: Bool = false) {
		self.topRefreshController.endRefreshing()
		self.tableFooterView = UIView(frame: .zero)
		self.loadMoreManager.loadMoreStatus = loadMoreStatus
	}
	
	@objc private func refreshTableView(_ sender: UIRefreshControl) {
		guard let refreshState = RefreshState(rawValue: sender.tag) else { return }
		
		switch refreshState {
		case .refresh:
			self.refreshManagerDelegate?.refresh()
		case .loadMore:
			self.loadMoreRefreshManagerDelegate?.loadMore()
		}
	}
	
	public func hideEmptyPlaceholder() {
		backgroundView = nil
	}
}

extension TableView: LoadMoreAnimationDelegate {
	
	public func startAnimating() {
		if totalNumberOfRows > 0 {
			self.tableFooterView = bottomView
			self.loadMoreRefreshManagerDelegate?.loadMore()
		} else {
			loadMoreManager.loadMoreStatus = false
		}
	}
	
}

public class LoadMoreManager {
	
	public weak var delegate: LoadMoreAnimationDelegate?
	
	public var loadMoreStatus = false
	
	public init() {}
	
	public func didScroll(scrollView: UIScrollView) {
		let currentOffset = scrollView.contentOffset.y
		let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
		let deltaOffset = maximumOffset - currentOffset

		if deltaOffset <= 20 {
			loadMore()
		}
	}
	
	public func loadMore() {
		if !loadMoreStatus {
			self.loadMoreStatus = true
			delegate?.startAnimating()
		}
	}
	
}
