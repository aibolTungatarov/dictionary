//
//  UIViewController+Extensions.swift
//  FDUIKit
//
//  Created by iBol on 5/13/21.
//

import UIKit
import SwiftMessages

public extension UIViewController {
	public func showUnderDevAlertView() {
		var config = SwiftMessages.Config()
		config.presentationStyle = .center
//		config.presentationContext = .window(windowLevel: .alert)
		config.duration = .forever
		config.dimMode = .gray(interactive: true)
		config.interactiveHide = true
		let view = UnderDevAlertView()
		view.didTapOkButtonClosure = { [weak self] in
//			self?.dismiss(animated: true)
			SwiftMessages.hide()
		}
		SwiftMessages.show(config: config, view: view)
	}
	
	public func showRequestSuccessAlertView() {
		var config = SwiftMessages.Config()
		config.presentationStyle = .center
		config.duration = .forever
		config.dimMode = .gray(interactive: true)
		config.interactiveHide = true
		let view = RequestSuccessAlertView()
		view.didTapOkButtonClosure = { [weak self] in
			SwiftMessages.hide()
		}
		SwiftMessages.show(config: config, view: view)
	}
}

public extension UIView {
	public func showUnderDevAlertView() {
		var config = SwiftMessages.Config()
		config.presentationStyle = .center
		config.presentationContext = .window(windowLevel: .alert)
		config.duration = .forever
		config.dimMode = .gray(interactive: true)
		config.interactiveHide = true
		let view = UnderDevAlertView()
		SwiftMessages.show(config: config, view: view)
	}
}
