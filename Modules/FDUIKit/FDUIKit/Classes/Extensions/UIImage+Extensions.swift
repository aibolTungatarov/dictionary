//
//  UIImage+Extensions.swift
//  FDUIKit
//
//  Created by iBol on 4/3/21.
//

import UIKit

public extension UIImage {
	public static func make(named: String, classForModuleBundle: AnyClass, bundleName: String) -> UIImage? {
		guard let bundlePath = Bundle(for: classForModuleBundle).path(forResource: bundleName,
																	  ofType: "bundle") else {
			return nil
		}
		let bundle = Bundle(path: bundlePath)
		
		return UIImage(named: named, in: bundle, compatibleWith: nil)
	}
}
