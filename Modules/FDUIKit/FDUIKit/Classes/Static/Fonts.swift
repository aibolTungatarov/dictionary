//
//  Fonts.swift
//  food_link
//
//  Created by Ramazan Kazybek on 3/14/21.
//

import UIKit

#if os(OSX)
import AppKit.NSFont
public typealias Font = NSFont
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIFont
public typealias Font = UIFont
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Fonts

// swiftlint:disable identifier_name line_length type_body_length
public enum FontFamily {
	public enum Roboto {
		public static let bold = FontConvertible(name: "Roboto-Bold", family: "Roboto", path: "Roboto-Bold.ttf")
		public static let medium = FontConvertible(name: "Roboto-Medium", family: "Roboto", path: "Roboto-Medium.ttf")
		public static let regular = FontConvertible(name: "Roboto-Regular", family: "Roboto", path: "Roboto-Regular.ttf")
		public static let all: [FontConvertible] = [bold, medium, regular]
	}
	
	//  internal enum OfficinaSans {
	//    internal static let regular = FontConvertible(name: "OfficinaSansCTT", family: "OfficinaSansCTT", path: "OfficinaSansCTT_33548.ttf")
	//    internal static let bold = FontConvertible(name: "OfficinaSansCTT-Bold", family: "OfficinaSansCTT", path: "OfficinaSansCTT-Bold_33545.ttf")
	//    internal static let all: [FontConvertible] = [regular, bold]
	//  }
	//  internal static let allCustomFonts: [FontConvertible] = [Roboto.all, OfficinaSans.all].flatMap { $0 }
	public static let allCustomFonts: [FontConvertible] = [Roboto.all].flatMap { $0 }
	public static func registerAllCustomFonts() {
		allCustomFonts.forEach { $0.register() }
	}
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

public struct FontConvertible {
	public let name: String
	public let family: String
	public let path: String
	
	public func font(size: CGFloat) -> Font! {
		return Font(font: self, size: size)
	}
	
	public func register() {
		// swiftlint:disable:next conditional_returns_on_newline
		guard let url = url else { return }
		CTFontManagerRegisterFontsForURL(url as CFURL, .process, nil)
	}
	
	fileprivate var url: URL? {
		guard let bundlePath = Bundle(for: BundleToken.self).path(forResource: "FDUIKit-Assets",
																	  ofType: "bundle") else {
			return nil
		}
		let bundle = Bundle(path: bundlePath)
		return bundle?.url(forResource: path, withExtension: nil)
	}
}

public extension Font {
	public convenience init?(font: FontConvertible, size: CGFloat) {
		#if os(iOS) || os(tvOS) || os(watchOS)
		if !UIFont.fontNames(forFamilyName: font.family).contains(font.name) {
			font.register()
		}
		#elseif os(OSX)
		if let url = font.url, CTFontManagerGetScopeForURL(url as CFURL) == .none {
			font.register()
		}
		#endif
		
		self.init(name: font.name, size: size)
	}
	
	public static func standard(size: CGFloat) -> Font {
		return FontFamily.Roboto.regular.font(size: size)
	}
}

private final class BundleToken {}
