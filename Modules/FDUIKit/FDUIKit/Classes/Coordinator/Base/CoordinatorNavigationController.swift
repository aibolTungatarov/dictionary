//
//  CoordinatorNavigationController.swift
//  KZSBUIKit
//
//  Created by Ramazan Kazybek on 11/24/20.
//

import UIKit

public protocol CoordinatorNavigationControllerDelegate: AnyObject {
	func transitionBackDidFinish()
	func customBackButtonDidTap()
	func customCloseButtonDidTap()
}

public class CoordinatorNavigationController: UINavigationController {
	weak var coordinatorNavigationDelegate: CoordinatorNavigationControllerDelegate?
	
	private var isPushBeingAnimated = false
	private let backBarButtonItem: UIBarButtonItem?
	
	init(backBarButtonItem: UIBarButtonItem?) {
		self.backBarButtonItem = backBarButtonItem
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		nil
	}
	
	override public func viewDidLoad() {
		super.viewDidLoad()
		delegate = self
		view.backgroundColor = .white
	}
	
	override public  func pushViewController(_ viewController: UIViewController, animated: Bool) {
		isPushBeingAnimated = true
		super.pushViewController(viewController, animated: animated)
		setupBackButton(viewController: viewController)
	}
	
	public func setupBackButton(viewController: UIViewController) {
		viewController.navigationItem.hidesBackButton = true
		viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Закрыть",
																		  style: .done,
																		  target: self,
																		  action: #selector(backButtonDidTap))
	}
	
	public func enableSwipeBack() {
		interactivePopGestureRecognizer?.isEnabled = true
		interactivePopGestureRecognizer?.delegate = self
	}
	
	@objc
	private func backButtonDidTap() {
		coordinatorNavigationDelegate?.customBackButtonDidTap()
	}
}

extension CoordinatorNavigationController: UINavigationControllerDelegate {
	public func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
		guard let coordinator = navigationController.topViewController?.transitionCoordinator else { return }
		coordinator.notifyWhenInteractionChanges { [weak self] context in
			guard !context.isCancelled else { return }
			self?.coordinatorNavigationDelegate?.transitionBackDidFinish()
		}
	}
	
	public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
		guard let swipeNavigationController = navigationController as? CoordinatorNavigationController else { return }
		swipeNavigationController.isPushBeingAnimated = false
	}
}

extension CoordinatorNavigationController: UIGestureRecognizerDelegate {
	public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
		guard gestureRecognizer == interactivePopGestureRecognizer else { return true }
		return viewControllers.count > 1 && isPushBeingAnimated == false
	}
	
	public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
								  shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
		true
	}
}
