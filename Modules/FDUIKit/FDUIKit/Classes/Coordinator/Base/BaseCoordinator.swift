//
//  BaseCoordinator.swift
//  KZSBUIKit
//
//  Created by Ramazan Kazybek on 11/24/20.
//

import UIKit

open class BaseCoordinator: Coordinator {
	public private(set) var childCoordinators = [Coordinator]()
	public let router: Router
	private var presentingCoordinator: ExitCoordinator?
	
	public init(router: Router) {
		self.router = router
	}
	
	open func start() { }
	open func start(with action: DeepLinkAction?) {}
	
	public func dismiss(child coordinator: Coordinator?) {
		router.dismissModule()
		removeDependency(coordinator)
	}
	
	public func addDependency(_ coordinator: Coordinator) {
		guard !childCoordinators.contains(where: { $0 === coordinator }) else { return }
		childCoordinators.append(coordinator)
	}
	
	public func removeDependency(_ coordinator: Coordinator?) {
		guard !childCoordinators.isEmpty,
			let coordinator = coordinator else { return }
		if let coordinator = coordinator as? BaseCoordinator, !coordinator.childCoordinators.isEmpty {
			coordinator.childCoordinators.filter { $0 !== coordinator }.forEach { coordinator.removeDependency($0) }
		}
		childCoordinators.removeAll { $0 === coordinator }
	}
	
	public func clearChildCoordinators() {
		childCoordinators.forEach { removeDependency($0) }
	}
}

extension BaseCoordinator {
	// MARK: - Actions
	
	@objc
	private func okButtonTapped(_ sender: UIButton) {
		switch RouterDismissType(rawValue: sender.tag) {
		case .dismiss: 				router.dismissModule()
		case .dismissPopToRoot: 	self.hideAlertAndFinishFlow()
		case .dismissPopModule: 	self.hideAlertAndPopModule()
		case .dismissFinishFlow:	self.hideAlertAndFinishFlow()
		case .none: 				break
		}
	}
	
	@objc
	private func brokenOkButtonTapped() {
		exit(0)
	}
	
	@objc
	public func hideAlertAndPopModule() {
		router.dismissModule { [weak self] in
			self?.router.popModule()
		}
	}
	
	@objc
	public func hideAlertAndFinishFlow() {
		router.dismissModule()
		if let presentingCoordinator = presentingCoordinator {
			presentingCoordinator.exitCompletion?()
			return
		}
		
		// Для ошибок которые прилетают с BaseCoordinator+NetworkError у которых нет ExitCoordinator
		(router.toPresent() as? UINavigationController)?.popToRootViewController(animated: true)
	}
}

public enum RouterDismissType: Int {
	case dismiss = 0
	case dismissPopModule = 1
	case dismissPopToRoot = 2
	case dismissFinishFlow = 3
}
