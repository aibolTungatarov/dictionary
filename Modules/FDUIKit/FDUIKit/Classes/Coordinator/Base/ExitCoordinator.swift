//
//  ExitCoordinator.swift
//  KZSBUIKit
//
//

import Foundation

public typealias CoordinatorOutput = (Coordinator & ExitCoordinator)

/// Протокол для выхода координатора с Flow
public protocol ExitCoordinator: AnyObject {
	/// Имплементация выхода
	var exitCompletion: Callback? { get set }
}
