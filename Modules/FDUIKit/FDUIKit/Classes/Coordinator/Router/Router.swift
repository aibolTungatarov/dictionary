//
//  Router.swift
//  AFNetworking
//
//  Created by Ramazan Kazybek on 11/24/20.
//

import UIKit

public typealias Callback = () -> Void

public final class Router: Presentable {
	private weak var rootController: UINavigationController?
	private var completions: [UIViewController: Callback]
	
	public init(rootController: UINavigationController) {
		self.rootController = rootController
		completions = [:]
	}
	
	public func toPresent() -> UIViewController? {
		rootController
	}
	
	public func dismissModule(animated: Bool = true, completion: Callback? = nil) {
		rootController?.dismiss(animated: animated, completion: completion)
	}
	
	public func dismissPresentedModule(_ presentedModule: Presentable?, animated: Bool = true, completion: Callback? = nil) {
		guard let controller = presentedModule?.toPresent() else { return }
		controller.dismiss(animated: true, completion: completion)
	}
	
	public func present(_ module: Presentable?, animated: Bool = true) {
		if #available(iOS 13.0, *) {
			present(module, animated: animated, modalPresentationStyle: .automatic)
		} else {
			present(module, animated: animated, modalPresentationStyle: .overFullScreen)
		}
	}
	
	public func present(_ module: Presentable?,
						animated: Bool = true,
						modalPresentationStyle: UIModalPresentationStyle,
						completion: Callback? = nil) {
		guard let controller = module?.toPresent() else { return }
		controller.modalPresentationStyle = modalPresentationStyle
		if #available(iOS 13.0, *) { controller.isModalInPresentation = true }
		rootController?.present(controller, animated: animated, completion: completion)
	}
	
	public func push(_ module: Presentable?, animated: Bool = true, hideBottomBarWhenPushed: Bool = true, completion: Callback? = nil) {
		guard let controller = module?.toPresent(), controller is UINavigationController == false else {
			assertionFailure("Deprecated push UINavigationController.")
			return
		}
		if let completion = completion {
			completions[controller] = completion
		}
		controller.hidesBottomBarWhenPushed = hideBottomBarWhenPushed
		rootController?.pushViewController(controller, animated: animated)
	}
	
	public func popModule(animated: Bool = true) {
		_ = rootController?.popViewController(animated: animated)
	}
	
	public func setRootModule(_ module: Presentable?, animated: Bool = false, isNavigationBarHidden: Bool = false) {
		guard let controller = module?.toPresent() else { return }
		rootController?.setViewControllers([controller], animated: animated)
		rootController?.isNavigationBarHidden = isNavigationBarHidden
	}
	
	public func changeRootModule(_ navigationController: UINavigationController, isNavigationBarHidden: Bool = false) {
		rootController = navigationController
		rootController?.isNavigationBarHidden = isNavigationBarHidden
	}
	
	public func popToRootModule(animated: Bool = true) {
		if let controllers = rootController?.popToRootViewController(animated: animated) {
			controllers.forEach(runCompletion)
		}
	}
	
	private func runCompletion(for controller: UIViewController) {
		guard let completion = completions[controller] else { return }
		completion()
		completions.removeValue(forKey: controller)
	}
}
