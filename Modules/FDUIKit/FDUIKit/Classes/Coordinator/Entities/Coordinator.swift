//
//  Coordinator.swift
//  AFNetworking
//
//  Created by Ramazan Kazybek on 11/24/20.
//

import Foundation

public protocol Coordinator: AnyObject {
	func start()
	func start(with action: DeepLinkAction?)
}

public extension Coordinator {
	func start(with action: DeepLinkAction?) {}
}

public protocol DeepLinkAction {}
