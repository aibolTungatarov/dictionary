//
//  UnderDevAlertView.swift
//  FDUIKit
//
//  Created by iBol on 5/13/21.
//

import UIKit

public protocol UnderDevAlertViewDelegate: AnyObject {
	func didTapOkButton()
}

public class UnderDevAlertView: UIView {
	public weak var delegate: UnderDevAlertViewDelegate?
	public var didTapOkButtonClosure: (() -> Void)?
	
	// MARK: - Views Factory
	private var logoImageView = UIImageView(image: .init(imageLiteralResourceName: "logo-small"))
	private let titleLabel =
		UILabelFactory(text: "Согласно расчету...")
		.font(FontFamily.Roboto.bold.font(size: 18))
		.text(alignment: .center)
		.text(color: .appBlack)
		.build()
	
	private let subtitleLabel =
		UILabelFactory(text: "Эта страница сейчас в разработке")
		.font(FontFamily.Roboto.bold.font(size: 14))
		.text(alignment: .center)
		.text(color: .appGray)
		.build()
	
	private let okButton: UIButton = {
		let view = UIButton.primaryUnderDevButton(title: "Хорошо")
		let shadowOffset = CGSize(width: 0, height: 4)
		view.addShadow(ofColor: .white, radius: 10, offset: shadowOffset, opacity: 0.1)
		view.tap(target: self, selector: #selector(didTapOkButton))
		return view
	}()
	
	private let alertView: UIView = {
		let view = UIView()
		view.corner(radius: 20)
		view.backgroundColor = .white
		return view
	}()
	
	public init() {
		super.init(frame: .zero)
		
		backgroundColor = .white
		addSubviews()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	@objc func didTapOkButton() {
		didTapOkButtonClosure?()
	}
}

extension UnderDevAlertView: Decoratable {
	public func addSubviews() {
		[alertView].forEach { addSubview($0) }
		[logoImageView, titleLabel, subtitleLabel, okButton].forEach { alertView.addSubview($0) }
	}
	
	public func setupLayout() {
		alertView.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(30)
			make.right.equalToSuperview().offset(-30)
			make.center.equalToSuperview()
		}
		
		logoImageView.snp.makeConstraints { make in
			make.width.height.equalTo(60)
			make.top.equalToSuperview().offset(60)
			make.centerX.equalToSuperview()
		}
		
		titleLabel.snp.makeConstraints { make in
			make.top.equalTo(logoImageView.snp.bottom).offset(25)
			make.left.equalToSuperview().offset(30)
			make.right.equalToSuperview().offset(-30)
			make.center.equalToSuperview()
		}
		
		subtitleLabel.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(20)
			make.right.equalToSuperview().offset(-20)
			make.top.equalTo(titleLabel.snp.bottom).offset(10)
		}
		
		okButton.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(20)
			make.right.equalToSuperview().offset(-20)
			make.top.equalTo(subtitleLabel.snp.bottom).offset(30)
			make.bottom.equalToSuperview().offset(-30)
			make.height.equalTo(50)
		}
	}
}

extension UIButton {
	internal func tap(target: Any?, selector: Selector) {
		self.addTarget(target, action: selector, for: .touchUpInside)
	}
	
	internal static func primaryUnderDevButton(title: String, target: Any, action: Selector) -> UIButton {
		let button = UIButton.primaryUnderDevButton(title: title)
		button.tap(target: target, selector: action)
		return button
	}
//
	internal static func primaryUnderDevButton(title: String, color: UIColor = .white, disabledColor: UIColor = .white, background: UIColor = .appOrange, disabledBackground: UIColor = .appGray, cornerRadius: CGFloat = 7, font: UIFont = FontFamily.Roboto.bold.font(size: 17)) -> UIButton {
		let button = UIButton(type: .custom)
		button.setTitle(title, for: .normal)
		button.setTitleColor(color, for: .normal)
		button.setTitleColor(disabledColor, for: .disabled)
		button.backgroundColor = .appOrange
		button.corner(radius: 7)
		button.clipsToBounds = true
		button.titleLabel?.font = font
		return button
	}
}
