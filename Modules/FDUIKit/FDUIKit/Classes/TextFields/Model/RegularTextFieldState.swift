//
//  RegularTextFieldState.swift
//  FDUIKit
//
//  Created by Ramazan Kazybek on 5/8/21.
//

import UIKit

public enum RegularTextFieldState {
	case error
	case success
	case selected
	case normal
	case disabled
	
	var backgroundColor: UIColor {
		switch self {
		case .error:
			return .red
		case .disabled:
			return UIColor.bubbleBackgroundGray.withAlphaComponent(0.4)
		default:
			return .white
		}
	}
	
	var borderColor: UIColor {
		switch self {
		case .error:
			return UIColor.red
		case .success:
			return UIColor.greenSea
		case .selected:
			return .appOrange
			
		default:
			return UIColor.lightGray
		}
	}
	
	var textColor: UIColor {
		switch self {
		case .disabled:
			return .lightGray
		default:
			return .appBlack
		}
	}
	
	var textFont: UIFont {
		return FontFamily.Roboto.regular.font(size: 14)
	}
}

