//
//  Decoratable.swift
//  food_link
//
//  Created by Ramazan Kazybek on 3/14/21.
//

public protocol Decoratable {
	func addSubviews()
	func setupLayout()
}
