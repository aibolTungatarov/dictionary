//
//  KeyboardPayload.swift
//  FDUtilities
//
//  Created by iBol on 5/10/21.
//

import Foundation
import UIKit

public struct KeyboardPayload {
	public let beginFrame: CGRect
	public let endFrame: CGRect
	public let curve: UIView.AnimationCurve
	public let duration: TimeInterval
	public let isLocal: Bool
}

extension KeyboardPayload {
	public init(note: Notification) {
		let userInfo = note.userInfo
		
		beginFrame = (userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? CGRect) ?? .zero
		endFrame = (userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect) ?? .zero
		curve = UIView.AnimationCurve(rawValue: (userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int) ?? 0) ?? .easeInOut
		duration = (userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval) ?? 0.4
		isLocal = (userInfo?[UIResponder.keyboardIsLocalUserInfoKey] as? Bool) ?? false
	}
}
