//
//  Typealiases.swift
//  FDUtilities
//
//  Created by iBol on 4/25/21.
//

import Foundation

public typealias ItemClosure<T> = ((T) -> Void)
public typealias OptionalItemClosure<T> = ((T?) -> Void)
public typealias VoidCompletion = (() -> ())
public typealias BoolClosure = ((Bool) -> ())
