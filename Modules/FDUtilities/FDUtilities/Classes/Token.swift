//
//  Token.swift
//  FDUtilities
//
//  Created by iBol on 4/25/21.
//

import Foundation

//enum TokenType: String, Codable {
//    case bearer = "Bearer"
//}

@objc
public class Token: NSObject, Codable {
	@objc public  var expireIn: Int64
	@objc public let refreshToken: String
	@objc public let authToken: String
	@objc public let tokenType: String
	
	public init(expireIn: Int64, refreshToken: String, authToken: String, tokenType: String) {
		self.expireIn = expireIn
		self.refreshToken = refreshToken
		self.authToken = authToken
		self.tokenType = tokenType
	}
	
	private enum CodingKeys: String, CodingKey {
		case expireIn = "expire_in"
		case refreshToken = "refresh_token"
		case authToken = "token"
		case tokenType = "token_type"
	}
	
	override public var description: String {
		return "EXPIRE IN: \(expireIn) \n REFRESH TOKEN: \(refreshToken) \n AUTH TOKEN: \(authToken)"
	}
	
	public func save() {
		let copy = self.copy() as! Self
		let newExpireIn = expireIn + Int64(Date().timeIntervalSince1970)
		copy.expireIn = newExpireIn

		UserDefaults.standard.token = copy
//        UserSessionManager.shared.token = authToken
	}
}

//    MARK: - NSCopying
extension Token: NSCopying {
	public func copy(with zone: NSZone? = nil) -> Any {
		let copy = Token(expireIn: expireIn, refreshToken: refreshToken, authToken: authToken, tokenType: tokenType)
		return copy
	}
}
