//
//  UICollectionViewExtensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 13.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    // MARK: - Methods
    public func register<T: UICollectionViewCell>(cellClass: T.Type) {
        self.register(cellClass, forCellWithReuseIdentifier: T.name)
    }
    
    func registerHeader<T: UICollectionReusableView>(aClass: T.Type) {
        register(aClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.name)
    }
    
    func registerFooter<T: UICollectionReusableView>(aClass: T.Type) {
        register(aClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.name)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.name, for: indexPath) as? T else { fatalError() }
        return cell
    }
    
    func dequeueReusableHeaderView<T: UICollectionReusableView>(for indexPath: IndexPath) -> T {
        guard let view = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                          withReuseIdentifier: T.name,
            for: indexPath) as? T else { fatalError() }

        return view
    }
    
    func dequeueReusableFooterView<T: UICollectionReusableView>(for indexPath: IndexPath) -> T {
        guard let view = dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionFooter,
                                                          withReuseIdentifier: T.name,
            for: indexPath) as? T else { fatalError() }

        return view
    }
}
