//
//  NavigationBar+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 27.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

public extension UINavigationBar {
    func makeClear() {
        if #available(iOS 13.0, *) {
            let navigationBarAppearance = UINavigationBarAppearance()
//            navigationBarAppearance.shadowColor = .clear
            navigationBarAppearance.configureWithTransparentBackground()
            navigationBarAppearance.backgroundColor = .white
//            navigationBarAppearance.backgroundEffect = nil
            
            standardAppearance = navigationBarAppearance
        } else {
            backgroundColor = .white
            setBackgroundImage(UIImage(), for: .default)
            shadowImage = UIImage()
        }
    }
    
	func changeBackgroundColor(to color: UIColor) {
        if #available(iOS 13.0, *) {
            standardAppearance.backgroundColor = color
        } else {
            backgroundColor = color
        }
    }
}
