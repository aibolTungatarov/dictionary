//
//  UIStackView+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 02.11.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

extension UIStackView {
    public func addArrangedSubviews(_ subviews: UIView...) {
        subviews.forEach {
            addArrangedSubview($0)
        }
    }
    
    public func addArrangedSubviews(_ subviews: [UIView]) {
        subviews.forEach {
            addArrangedSubview($0)
        }
    }
    
    public func removeAllArrangedSubviews() {
        arrangedSubviews.forEach {
            $0.removeFromSuperview()
        }
    }
}
