//
//  WebView+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 25.09.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import WebKit

extension WKWebView {
    public func cleanAllCookies() {
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        print("All cookies deleted")
        
//        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
//            records.forEach { record in
//                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
//                print("Cookie ::: \(record) deleted")
//            }
//        }
    }
    
    public func setAuthCookies() {
        guard let token = UserDefaults.standard.token else { return }
        
        let authAccessCookie = HTTPCookie(properties: [
            .domain: "Environment.domainURL",
            .path: "/",
            .name: "auth_access",
            .value: token.authToken
        ])!
        
        configuration.websiteDataStore.httpCookieStore.setCookie(authAccessCookie)
    }
}
