//
//  Globals.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 27.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

var appVersion: String {
    guard let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else {
        return "1.0"
    }
    
    return version
}

let appId = "597833321"
let appUrl = String(format: "itms-apps://itunes.apple.com/app/id%@", appId)

func deviceModel() -> String {
    var systemInfo = utsname()
    uname(&systemInfo)
    let machineMirror = Mirror(reflecting: systemInfo.machine)
    return machineMirror.children.reduce("") { identifier, element in
        guard let value = element.value as? Int8, value != 0 else { return identifier }
        return identifier + String(UnicodeScalar(UInt8(value)))
    }
}

func convertArrayToDictionary(key: String, _ array: [String]) -> [String: String] {
    var dictionary: [String: String] = [:]
    
    for i in 0..<array.count {
        dictionary[key + "[\(i)]"] = array[i]
    }
    
    return dictionary
}
