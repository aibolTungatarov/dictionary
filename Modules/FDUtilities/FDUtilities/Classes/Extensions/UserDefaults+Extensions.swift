import Foundation

public extension UserDefaults {
	public var collectingNumbersRequestsCount: Int {
		get {
			return integer(forKey: #function) < 0 ? 0 : integer(forKey: #function)
		}
		set {
			set(newValue, forKey: #function)
		}
	}
	
	public var shouldAskUserForCollectingNumbers: Bool {
		get {
			return bool(forKey: #function)
		}
		set {
			set(newValue, forKey: #function)
		}
	}

	public var shouldReceivePushNotifications: Bool {
		get {
			return bool(forKey: #function)
		}
		set {
			set(newValue, forKey: #function)
		}
	}
	
	public var isUserAuthed: Bool {
		return token != nil
	}
	
	public var token: Token? {
		get {
			if let tokenData = object(forKey: #function) as? Data {
				let decoder = JSONDecoder()
				if let token = try? decoder.decode(Token.self, from: tokenData) {
					return token
				}
			}
			
			return nil
		}
		set {
			let encoder = JSONEncoder()
			let encoded = try? encoder.encode(newValue)
			set(encoded, forKey: #function)
		}
	}

	
//	var userProfile: User? {
//		get {
//			if let profileData = object(forKey: #function) as? Data {
//				let decoder = JSONDecoder()
//				if let profile = try? decoder.decode(User.self, from: profileData) {
//					return profile
//				}
//			}
//
//			return nil
//		}
//		set {
//			let encoder = JSONEncoder()
//			if let profile = newValue, let encoded = try? encoder.encode(profile) {
//				set(encoded, forKey: #function)
//			}
//		}
//	}
	
	public var appWasLaunchedBefore: Bool {
		get {
			return bool(forKey: #function)
		}
		set {
			set(newValue, forKey: #function)
		}
	}
	
	public var pushToken: String? {
		get {
			return string(forKey: #function)
		}
		set {
			set(newValue, forKey: #function)
		}
	}
	
	var userNotificationsCount: Int {
		get {
			return integer(forKey: #function)
		}
		set {
			set(newValue, forKey: #function)
		}
	}
	
	var userReviewsCount: Int {
		get {
			return integer(forKey: #function)
		}
		set {
			set(newValue, forKey: #function)
		}
	}
	
	var didShowChatHint: Bool {
		get {
			return bool(forKey: #function)
		}
		set {
			set(newValue, forKey: #function)
		}
	}
}

class DefaultsKeys { }

final class DefaultsKey<T>: DefaultsKeys {
	let value: String

	init(_ value: String) {
		self.value = value
	}
}

extension DefaultsKeys {
	static let lastReviewRequestAppVersion = DefaultsKey<String>("lastReviewRequestAppVersion")
}

extension UserDefaults {
	func get<T>(forKey key: DefaultsKey<T>) -> T? {
		return object(forKey: key.value) as? T
	}

	func set<T>(_ value: T?, forKey key: DefaultsKey<T>) {
		set(value, forKey: key.value)
	}
}
