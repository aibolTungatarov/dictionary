//
//  String+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 19.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

private let phoneNumberValidLength = 11

public extension String {
    
    /**
        Булевое значение, которое говорит: имеет ли данная строка верный формат номер телефона или нет. Проверка осуществляется исключением из строки все лишние символы (скобки, тире, знак плюса, пробелы и тд), проверкой на наличие только численных символов, а также сравниванием форматированной строки на валидную длину номера телефона.
    */
    
    var isValidPhone: Bool {
        return formattedPhone.count == phoneNumberValidLength && formattedPhone.isNumberic
    }
    
    /**
        Булевое значение, которое говорит: имеет ли данная строка верный формат электронной почты или нет.
    */
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailPredicate.evaluate(with: self)
    }
    
    /**
        Строка в виде отформаттированного номера телефона. Отформатированный номер телефона - строка без, состоящая только из цифр, без скобок, пробелов, знака плюс и тд. Предназначение - для отправки на бэкэнд.
    */
    var formattedPhone: String {
        let excludedCharacters = [" ", "+", "(", ")", "-"]
        let filteredPhone = self.filter({ !excludedCharacters.contains(String($0)) })
        let result = _replaceFirstDigitIfNeeded(in: filteredPhone)
        
        return result
    }
    
    /**
        Отформатированная строка. Форматируется в зависимости от типа логина.
    */
    var formattedLogin: String {
        return isValidPhone ? formattedPhone : self
    }
    
    /**
        Строка, которая содержит в себе номер телефона, но в формате, который необходим для отображения пользователю (со всеми скобками, пробелами, знаком плюс и тд).
    */
    var userPresentablePhone: String {
        if self.isValidPhone {
            let phone = formattedPhone
            return String(format: "+%@ (%@) %@-%@-%@", arguments: [
                phone.substring(with: 0..<1),
                phone.substring(with: Range(1...3)),
                phone.substring(with: Range(4...6)),
                phone.substring(with: Range(7...8)),
                phone.substring(with: Range(9...10))
            ])
        } else {
            return self
        }
    }
    
    /// Заменяет первую цифру в номере телефона, если она равна 8.
    /// Так как для отправки на бэкэнд, номер телефона должен начинаться именно с цифры 7.
    ///
    /// - Parameter phone: Отформатированный номер телефона (без скобок, пробелов, знака плюс и тд).
    /// - Returns: Номер телефона с изменённой первой цифра, если это 8.
    private func _replaceFirstDigitIfNeeded(in phone: String) -> String {
        if phone.count >= 1 && phone.substring(to: 1) == "8" {
            return "7" + phone.substring(from: 1)
        }
        
        return phone
    }
}

extension String {
    subscript (bounds: CountableClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }

    subscript (bounds: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: bounds.lowerBound)
        let end = index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }
}

public extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }
    
    func substring(with range: Range<Int>) -> String {
        let startIndex = index(from: range.lowerBound)
        let endIndex = index(from: range.upperBound)
        return String(self[startIndex..<endIndex])
    }
    
    var doubleValue: Double {
        return Double(self) ?? 0.0
    }
    
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return NSMutableAttributedString() }
        do {
            return try NSMutableAttributedString(data: data, options: [.documentType: NSMutableAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSMutableAttributedString()
        }
    }
    
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    var youtubeID: String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
    
    func getDate(from format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        
        return date
    }
    
     func getDateString(with dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = dateFormat
        
        if let date = date {
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
        
        return self
    }
    
    func getDateString(from dateFormat: String, to format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = format
        
        if let date = date {
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
        
        return self
    }
    
    public var relativeTimeText: String? {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
        let now = Date()
        guard let currentYear = Int(dateFormatter.string(from: now).getDateString(with: "yyyy")),
            let givenDateYear = Int(getDateString(with: "yyyy")),
            let currentMonth = Int(dateFormatter.string(from: now).getDateString(with: "MM")),
            let givenDateMonth = Int(getDateString(with: "MM")),
            let currentDay = Int(dateFormatter.string(from: now).getDateString(with: "d")),
            let givenDateDay = Int(getDateString(with: "d")) else { return getDateString(with: "dd.MM.yyyy") }
        
        guard currentYear == givenDateYear else { return getDateString(with: "dd.MM.yyyy") }
        guard currentMonth == givenDateMonth else { return getDateString(with: "dd.MM") }
        
        switch currentDay - givenDateDay {
        case 0: return getDateString(with: "HH:mm")
        case 1: return "вчера"
        case 2...6: return getDateString(with: "EEEE")
        default: return getDateString(with: "dd.MM")
        }
    }
    
    func width(using font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }

    func height(using font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.height
    }

    func size(using font: UIFont) -> CGSize {
        let fontAttributes = [NSAttributedString.Key.font: font]
        return self.size(withAttributes: fontAttributes)
    }
    
    var isNumberic: Bool {
        let letters = filter { $0.isLetter }
        return letters.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
}
