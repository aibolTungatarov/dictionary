//
//  FloatingPoints+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 03.11.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

extension FloatingPoint {
    var toRadians: Self { return self * .pi / 180 }
    var toDegree: Self { return self * 180 / .pi }
}
