//
//  UIImageView+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 21.02.2021.
//  Copyright © 2021 chocolife.me. All rights reserved.
//

import UIKit

extension UIImageView {
  func setImageColor(_ color: UIColor) {
    let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
    self.image = templateImage
    self.tintColor = color
  }
}
