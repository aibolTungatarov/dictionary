//
//  Thread+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 09.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

extension Thread {
    public class func onMainThread(_ completion: @escaping (() -> Void)) {
        if Thread.current.isMainThread {
            completion()
            return
        } else {
            DispatchQueue.main.async {
                completion()
            }
        }
    }
}
