//
//  UIRefreshControl+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 07.11.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

extension UIRefreshControl {
    /// Этот метод позволяет вручную послать тригер для отображения refersh control.
    /// - Parameter shouldSendActions: По умолчанию этот параметр = true, что означает, что автоматически будет послан
    /// запрос, который описан в методе refresh() в протоколе RefreshManagerDelegate
    func beginRefreshingManually(shouldSendActions: Bool = true) {
        guard !isRefreshing else { return }
        
        if let scrollView = superview as? UIScrollView {
            let offsetPoint = CGPoint(x: 0, y: -(frame.size.height + 8))
            scrollView.setContentOffset(offsetPoint, animated: true)
        }
        beginRefreshing()
        
        if shouldSendActions {
            sendActions(for: .valueChanged)
        }
    }
}
