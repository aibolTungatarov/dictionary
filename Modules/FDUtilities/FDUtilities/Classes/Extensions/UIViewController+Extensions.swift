//
//  UIViewController+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 19.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit
import SwiftMessages
import SVProgressHUD

public extension UIViewController {

    func showAlert(with title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)

        alertController.addAction(okAction)
        alertController.popoverPresentationController?.sourceView = view
        present(alertController, animated: true, completion: nil)
    }

    func showAlert(with title: String?, message: String?, _ completion: @escaping () -> ()) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
            self?.dismiss(animated: true, completion: completion)
        })

        alertController.addAction(okAction)
        alertController.popoverPresentationController?.sourceView = self.view
        present(alertController, animated: true, completion: nil)
    }

    func resetBackButton() {
        let backButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back"), style: .plain, target: self, action: #selector(backButtonDidPress))
        navigationItem.leftBarButtonItem = backButtonItem
    }

    @objc private func backButtonDidPress(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }

    func enableInteractionEvents() {
        UIApplication.shared.endIgnoringInteractionEvents()
    }

    func disableInteractionEvents() {
        UIApplication.shared.beginIgnoringInteractionEvents()
    }

    func shareActivity(items: [Any]) {
        let shareController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        shareController.excludedActivityTypes = [.airDrop, .assignToContact, .openInIBooks]
        shareController.popoverPresentationController?.sourceView = view
        shareController.popoverPresentationController?.sourceRect = view.bounds

        present(shareController, animated: true, completion: nil)
    }

    /**
     Alert controller for presenting with actions and with default CANCEL action
     */
    @nonobjc
    func showAlert(title: String? = nil, message: String? = nil, preferredStyle: UIAlertController.Style, actions: UIAlertAction...) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)

        actions.forEach({ alertController.addAction( $0 ) })

        if actions.filter({ $0.style == .cancel }).count == 0 {
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
        }

        alertController.popoverPresentationController?.sourceView = view
        present(alertController, animated: true, completion: nil)
    }

    @nonobjc
    func showAlert(title: String? = nil, message: String? = nil, preferredStyle: UIAlertController.Style, actions: [UIAlertAction]) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)

        actions.forEach({ alertController.addAction( $0 ) })

        if actions.filter({ $0.style == .cancel }).count == 0 {
            let cancelAction = UIAlertAction(title: "Отмена", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
        }

        alertController.popoverPresentationController?.sourceView = view
        Thread.onMainThread { [weak self] in
            self?.present(alertController, animated: true, completion: nil)
        }
    }

    @nonobjc
    func add(_ child: UIViewController, frame: CGRect? = nil) {
        addChild(child)

        view.addSubview(child.view)
        if let frame = frame {
            child.view.frame = frame
        } else {
            child.view.fillSuperview()
        }
        child.didMove(toParent: self)
    }

    func remove() {
        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }

    func addTapGestureToDismissKeyboard() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapToDismiss(_:)))
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.numberOfTapsRequired = 1

        view.addGestureRecognizer(tapGesture)
    }

    @objc private func handleTapToDismiss(_ gesture: UITapGestureRecognizer) {
        view.endEditing(true)
    }

    @objc func showBanner(title: String?, subtitle: String? = nil) {
        DispatchQueue.main.async {
            let banner = MessageView.viewFromNib(layout: .cardView)
            banner.configureTheme(.success)
            banner.titleLabel?.numberOfLines = 0
            banner.button?.removeFromSuperview()
            banner.configureDropShadow()
            banner.configureContent(title: title ?? "", body: subtitle ?? "")
            SwiftMessages.show(view: banner)
        }
    }

    func showError(with errorMessage: String? = nil) {
        DispatchQueue.main.async {
            let error = MessageView.viewFromNib(layout: .cardView)
            error.configureTheme(.error)
            error.button?.removeFromSuperview()
            error.configureDropShadow()
            error.configureContent(title: "Ошибка", body: errorMessage ?? "")
            SwiftMessages.show(view: error)
        }
    }

    func showProgress() {
        SVProgressHUD.show()
    }

    func dismissProgress() {
        SVProgressHUD.dismiss()
    }
}

// MARK: - KeyboardApperance
extension UIViewController {
    private struct NotificationTokens {
        private static var tokens: [NotificationToken] = []

        static func append(_ notificationToken: NotificationToken) {
            tokens.append(notificationToken)
        }

        static func removeAll() {
            tokens.removeAll()
        }
    }

	public final func registerKeyboardNotifications(onShow: @escaping (KeyboardPayload) -> (), onHide: @escaping (KeyboardPayload) -> ()) {
        let center = NotificationCenter.default
        let keyboardWillShowToken = center.addObserver(with: UIViewController.keyboardWillShow) { [weak self] (payload) in
            guard let self = self else { return }

            self.disableInteractionEvents()
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: payload.duration, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
                guard let self = self else { return }

                onShow(payload)

                self.view.layoutIfNeeded()
            }, completion: { [weak self] (position) in
                if position == .end {
                    guard let self = self else { return }
                    self.enableInteractionEvents()
                }
            })
        }
        let keyboardWillHideToken = center.addObserver(with: UIViewController.keyboardWillHide) { [weak self] (payload) in
            guard let self = self else { return }

            self.disableInteractionEvents()
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: payload.duration, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
                guard let self = self else { return }

                onHide(payload)

                self.view.layoutIfNeeded()
            }, completion: { [weak self] (position) in
                if position == .end {
                    guard let self = self else { return }
                    self.enableInteractionEvents()
                }
            })
        }

        NotificationTokens.append(keyboardWillShowToken)
        NotificationTokens.append(keyboardWillHideToken)
    }
	
    public final func unregisterKeyboardNotifications() {
        NotificationTokens.removeAll()
    }
}
