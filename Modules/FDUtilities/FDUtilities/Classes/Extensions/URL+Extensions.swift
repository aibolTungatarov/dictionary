//
//  URL+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 09.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

extension URL {
    public var queryParameters: [String: String]? {
        guard
            let components = URLComponents(url: self, resolvingAgainstBaseURL: true),
            let queryItems = components.queryItems else { return nil }
        return queryItems.reduce(into: [String: String]()) { (result, item) in
            result[item.name] = item.value
        }
    }
    
    public var isExternalLink: Bool {
        guard let queryParameterKeys = queryParameters?.keys else { return false }
        for queryKey in queryParameterKeys {
            return queryKey.contains("external") && queryParameters?[queryKey] == "1"
        }
        
        return false
    }
    
    public var isRahmetLink: Bool {
        return absoluteURL.absoluteString.contains("rahmetapp://")
    }
    
    public var isNativeDealLink: Bool {
        guard let urlComponents = URLComponents(string: absoluteString) else { return false }
        guard let queryItems = urlComponents.queryItems else { return false }
        
        for queryItem in queryItems {
            if queryItem.name == "open_action" && queryItem.value == "general" {
                return true
            }
        }
        
        return false
    }
}
