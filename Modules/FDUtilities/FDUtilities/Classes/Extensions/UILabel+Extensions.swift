//
//  UILabel+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 10.07.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import UIKit

extension UILabel {
    func lineSpacing(_ spacingValue: CGFloat = 2) {

        guard let textString = text else { return }
        let attributedString = NSMutableAttributedString(string: textString)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = spacingValue
        attributedString.addAttribute(
            .paragraphStyle,
            value: paragraphStyle,
            range: NSRange(location: 0, length: attributedString.length
        ))
        attributedText = attributedString
    }
    
//    // MARK: - Highlight Label
//    static func highlight(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .appOrange
//        label.font = FontFamily.Roboto.medium.font(size: fontSize)
//        return label
//    }
//
//    static func highlightRegular(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .appOrange
//        label.font = FontFamily.Roboto.regular.font(size: fontSize)
//        return label
//    }
//
//    static func highlightBold(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .appOrange
//        label.font = FontFamily.Roboto.bold.font(size: fontSize)
//        return label
//    }
    
//    static func hintRegular(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .interfaceGray
//        label.font = FontFamily.Roboto.regular.font(size: fontSize)
//        return label
//    }
//    
//    static func hintMedium(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .interfaceGray
//        label.font = FontFamily.Roboto.medium.font(size: fontSize)
//        return label
//    }
//    
//    static func placeholderRegular(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .darkGray
//        label.font = FontFamily.Roboto.regular.font(size: fontSize)
//        return label
//    }
//    
//    static func bodyRegular(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .appBlack
//        label.font = FontFamily.Roboto.regular.font(size: fontSize)
//        return label
//    }
//    
//    static func bodyMedium(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .appBlack
//        label.font = FontFamily.Roboto.medium.font(size: fontSize)
//        return label
//    }
//
//    static func bodyBold(_ fontSize: CGFloat, lines: Int = 1) -> UILabel {
//        let label = UILabel()
//        label.lineBreakMode = .byTruncatingTail
//        label.numberOfLines = lines
//        label.textColor = .appBlack
//        label.font = FontFamily.Roboto.bold.font(size: fontSize)
//        return label
//    }
}
