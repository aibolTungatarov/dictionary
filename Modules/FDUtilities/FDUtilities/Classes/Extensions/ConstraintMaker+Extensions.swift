//
//  ConstraintMaker+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 19.08.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation
import SnapKit

extension ConstraintMaker {
    public func aspectRatio(_ x: Double, by y: Double, self instance: ConstraintView) {
        self.width.equalTo(instance.snp.height).multipliedBy(x / y)
    }
}
