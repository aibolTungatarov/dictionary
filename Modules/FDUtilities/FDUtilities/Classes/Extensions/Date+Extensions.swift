//
//  Date+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 03.11.2020.
//  Copyright © 2020 Zhenya Kim. All rights reserved.
//

import Foundation

extension Date {
    func string(with dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        
        return dateFormatter.string(from: self)
    }
}
