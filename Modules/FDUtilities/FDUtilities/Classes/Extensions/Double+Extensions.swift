//
//  Double+Extensions.swift
//  ChocolifeMerchant
//
//  Created by Aibol on 24.02.2021.
//  Copyright © 2021 chocolife.me. All rights reserved.
//

import Foundation

extension Double {
    func toInt() -> Int? {
        if self >= Double(Int.min) && self < Double(Int.max) {
            return Int(self)
        } else {
            return nil
        }
    }
}

