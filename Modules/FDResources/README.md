# FDResources

[![CI Status](https://img.shields.io/travis/iBol/FDResources.svg?style=flat)](https://travis-ci.org/iBol/FDResources)
[![Version](https://img.shields.io/cocoapods/v/FDResources.svg?style=flat)](https://cocoapods.org/pods/FDResources)
[![License](https://img.shields.io/cocoapods/l/FDResources.svg?style=flat)](https://cocoapods.org/pods/FDResources)
[![Platform](https://img.shields.io/cocoapods/p/FDResources.svg?style=flat)](https://cocoapods.org/pods/FDResources)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDResources is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDResources'
```

## Author

iBol, aibolseed@gmail.com

## License

FDResources is available under the MIT license. See the LICENSE file for more info.
