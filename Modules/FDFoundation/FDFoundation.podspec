#
# Be sure to run `pod lib lint FDFoundation.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FDFoundation'
  s.version          = '0.1.0'
  s.summary          = 'A short description of FDFoundation.'
  s.homepage         = 'https://github.com/iBol/FDFoundation'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'iBol' => 'aibolseed@gmail.com' }
  s.source           = { :git => 'https://github.com/iBol/FDFoundation.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'FDFoundation/Classes/**/*'
	s.dependency 'KeychainAccess'
	s.dependency 'EasyDi'
	s.dependency 'PromisesSwift'
end
