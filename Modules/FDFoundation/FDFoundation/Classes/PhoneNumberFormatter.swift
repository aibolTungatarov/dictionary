//
//  PhoneNumberFormatter.swift
//  FDFoundation
//
//  Created by Ramazan Kazybek on 5/13/21.
//

private enum Constants {
	static let digitsCount = 11
	static let codePrefix = "+7"
}

public extension String {
	/// Конвертирует телефонный номер в API-формат(10-значный, только цифры)
	/// Пример: 77076420271
	public func rawPhoneNumber() -> String? {
		let text = self
		let digits = text
			.unicodeScalars
			.filter { CharacterSet.decimalDigits.contains($0) }
			.map { String($0) }
			.joined()
		return digits
	}
}
