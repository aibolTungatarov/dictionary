//
//  BiometricAuthServiceKit.swift
//  FDFoundation
//
//  Created by Ramazan Kazybek on 4/29/21.
//

import EasyDi

final public class BiometricAuthServiceKit: Assembly {
	public var service: BiometricAuthService {
		define(scope: Scope.lazySingleton, init: BiometricAuthService())
	}
}
