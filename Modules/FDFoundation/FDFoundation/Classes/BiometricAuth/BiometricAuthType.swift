//
//  BiometricAuthType.swift
//  FDFoundation
//
//  Created by Ramazan Kazybek on 4/26/21.
//

public enum BiometricAuthType {
	case none
	case touchId
	case faceId
	
	public var title: String? {
		switch self {
		case .faceId:
			return "Face ID"
		case .touchId:
			return "Touch ID"
		case .none:
			return nil
		}
	}
}

