//
//  UserSessionStorage.swift
//  FDFoundation
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import EasyDi

public final class UserSessionStorageKit: Assembly {
	public var storage: UserSessionStorage {
		define(scope: Scope.lazySingleton, init: UserSessionStorage())
	}
}

public final class UserSessionStorage {
	@KeychainEntry("accessToken")
	public var accessToken: String?

	@KeychainEntry("refreshToken")
	public var refreshToken: String?
	
	@KeychainEntry("phoneNumber")
	public var phoneNumber: String?
	
	@KeychainEntry("name")
	public var name: String?
	
	@KeychainEntry("surname")
	public var surname: String?
	
	@KeychainEntry("address")
	public var address: String?

	@KeychainEntry("pin")
	public var pin: String?

	@KeychainEntry("otpId")
	public var otpId: String?

	@UserDefaultsEntry("isBiometricAuthBeingUsed", defaultValue: false)
	public var isBiometricAuthBeingUsed: Bool
	
	@UserDefaultsEntry("numberOfUnreadNotificationsMessages", defaultValue: 0)
	public var numberOfUnreadNotificationsMessages: Int

	public init() {}

	public func clearAll() {
		accessToken = nil
		refreshToken = nil
		phoneNumber = nil
		name = nil
		surname = nil
		address = nil
		pin = nil
		otpId = nil
		isBiometricAuthBeingUsed = false
	}
}
