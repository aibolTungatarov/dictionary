//
//  Array+Safe.swift
//  Authorization
//
//  Created by Ramazan Kazybek on 4/25/21.
//

import Foundation

public extension Collection {
	subscript (safe index: Index) -> Element? {
		return indices.contains(index) ? self[index] : nil
	}
}
