# FDFoundation

[![CI Status](https://img.shields.io/travis/iBol/FDFoundation.svg?style=flat)](https://travis-ci.org/iBol/FDFoundation)
[![Version](https://img.shields.io/cocoapods/v/FDFoundation.svg?style=flat)](https://cocoapods.org/pods/FDFoundation)
[![License](https://img.shields.io/cocoapods/l/FDFoundation.svg?style=flat)](https://cocoapods.org/pods/FDFoundation)
[![Platform](https://img.shields.io/cocoapods/p/FDFoundation.svg?style=flat)](https://cocoapods.org/pods/FDFoundation)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDFoundation is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDFoundation'
```

## Author

iBol, aibolseed@gmail.com

## License

FDFoundation is available under the MIT license. See the LICENSE file for more info.
