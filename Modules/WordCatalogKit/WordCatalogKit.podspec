#
# Be sure to run `pod lib lint WordCatalogKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'WordCatalogKit'
  s.version          = '0.1.0'
  s.summary          = 'A short description of WordCatalogKit.'
  s.homepage         = 'https://github.com/iBol/WordCatalogKit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'iBol' => 'Aybol.Tungatarov@sberbank.kz' }
  s.source           = { :git => 'https://github.com/iBol/WordCatalogKit.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'WordCatalogKit/Classes/**/*'
	s.dependency 'EasyDi'
	s.dependency 'Alamofire'
	s.dependency 'FDNetworkLayer'
	s.dependency 'PromisesSwift'
end
