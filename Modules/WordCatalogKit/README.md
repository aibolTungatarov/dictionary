# WordCatalogKit

[![CI Status](https://img.shields.io/travis/iBol/WordCatalogKit.svg?style=flat)](https://travis-ci.org/iBol/WordCatalogKit)
[![Version](https://img.shields.io/cocoapods/v/WordCatalogKit.svg?style=flat)](https://cocoapods.org/pods/WordCatalogKit)
[![License](https://img.shields.io/cocoapods/l/WordCatalogKit.svg?style=flat)](https://cocoapods.org/pods/WordCatalogKit)
[![Platform](https://img.shields.io/cocoapods/p/WordCatalogKit.svg?style=flat)](https://cocoapods.org/pods/WordCatalogKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WordCatalogKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WordCatalogKit'
```

## Author

iBol, Aybol.Tungatarov@sberbank.kz

## License

WordCatalogKit is available under the MIT license. See the LICENSE file for more info.
