//
//  SearchRequest.swift
//  WordCatalogKit
//
//  Created by iBol on 8/8/21.
//

import Foundation
import FDNetworkLayer
import Alamofire

/// Получение списка карт
public struct SearchRequest: INetworkRequest {
	public typealias RequestModel = SearchModel.Request
	public typealias ResponseModel = SearchModel.Response
	
	var requestModel: RequestModel
	
	public init(requestModel: RequestModel) {
		self.requestModel = requestModel
	}
	
	public var path: String {
		return "/api/public/v1/words/search"
	}
	
	public var queryItems: [URLQueryItem]? {
		return [
			.init(name: "search", value: requestModel.search),
			.init(name: "page", value: requestModel.page),
			.init(name: "pageSize", value: requestModel.pageSize)
		]
	}
	
	public var method: NetworkMethod {
		return .GET
	}

	public var body: RequestModel {
		return requestModel
	}
	
	public var parameters: Parameters? {
		return nil
	}
}

public enum SearchModel {
	public struct Request: Codable {
		public let search: String?
		public let page: String?
		public let pageSize: String?
		
		public init(search: String?, page: String? = nil, pageSize: String? = nil) {
			self.search = search
			self.page = page
			self.pageSize = pageSize
		}
	}
	
	public struct Response: Decodable {
		public let id: Int?
		public let text: String?
		public let meanings: [Meaning]?
	}
}
