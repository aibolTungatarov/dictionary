//
//  Meaning.swift
//  WordCatalogKit
//
//  Created by iBol on 8/8/21.
//

import Foundation

public struct Meaning: Decodable {
	public let id: Int?
	public let translation: Translation?
	public let previewUrl: String?
	public let imageUrl: String?
	public let transcription: String?
	public let soundUrl: String?
}
