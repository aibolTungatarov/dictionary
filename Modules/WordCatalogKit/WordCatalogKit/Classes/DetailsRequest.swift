//
//  DetailsRequest.swift
//  WordCatalogKit
//
//  Created by iBol on 8/9/21.
//

import Foundation
import FDNetworkLayer
import Alamofire

/// Получение списка карт
public struct DetailsRequest: INetworkRequest {
	public typealias RequestModel = DetailsModel.Request
	public typealias ResponseModel = DetailsModel.Response
	
	var requestModel: RequestModel
	
	public init(requestModel: RequestModel) {
		self.requestModel = requestModel
	}
	
	public var path: String {
		return "/api/public/v1/meanings"
	}
	
	public var queryItems: [URLQueryItem]? {
		return [
			.init(name: "ids", value: requestModel.ids)
		]
	}
	
	public var method: NetworkMethod {
		return .GET
	}

	public var body: RequestModel {
		return requestModel
	}
	
	public var parameters: Parameters? {
		return nil
	}
}

public enum DetailsModel {
	public struct Request: Codable {
		public let ids: String?
		
		public init(ids: String?) {
			self.ids = ids
		}
	}
	
	public struct Response: Decodable {
		public let id: String?
		public let text: String?
		public let soundUrl: String?
		public let transcription: String?
		public let translation: Translation?
		public let images: [DictionaryImage]?
		public let definition: WordDefinition?
		public let examples: [WordExample]?
	}
}
