//
//  Translation.swift
//  WordCatalogKit
//
//  Created by iBol on 8/8/21.
//

import Foundation

public struct Translation: Decodable {
	public let text: String?
	public let note: String?
}
