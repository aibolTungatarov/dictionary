//
//  SearchKitAssembly.swift
//  WordCatalogKit
//
//  Created by iBol on 8/8/21.
//

import EasyDi
import FDNetworkLayer

public final class SearchKitAssembly: Assembly {
	// MARK: - Params
	let provider: ProviderAssembly = DIContext.defaultInstance.assembly()
	
	// MARK: - service
	public var service: SearchService {
		return define(scope: Scope.lazySingleton, init: self.provider.service)
	}
}
