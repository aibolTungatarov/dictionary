//
//  SearchService.swift
//  WordCatalogKit
//
//  Created by iBol on 8/8/21.
//

import Promises
import FDNetworkLayer

public protocol SearchService {
	func searchTranslation(_ request: SearchRequest) -> Promise<[SearchModel.Response]>
	func getDetails(_ request: DetailsRequest) -> Promise<[DetailsModel.Response]>
}

extension Provider: SearchService {
	public func searchTranslation(_ request: SearchRequest) -> Promise<[SearchModel.Response]> {
		rest.get(request.fullPath ?? "", parameters: request.parameters)
	}
	
	public func getDetails(_ request: DetailsRequest) -> Promise<[DetailsModel.Response]> {
		rest.get(request.fullPath ?? "", parameters: request.parameters)
	}
}
