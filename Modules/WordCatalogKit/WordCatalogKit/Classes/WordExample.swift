//
//  WordExample.swift
//  WordCatalogKit
//
//  Created by iBol on 8/9/21.
//

import Foundation

public struct WordExample: Decodable {
	public let text: String?
	public let soundUrl: String?
}
