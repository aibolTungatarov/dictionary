//
//  DictionaryImage.swift
//  WordCatalogKit
//
//  Created by iBol on 8/9/21.
//

import Foundation

public struct DictionaryImage: Decodable {
	public let url: String?
}
