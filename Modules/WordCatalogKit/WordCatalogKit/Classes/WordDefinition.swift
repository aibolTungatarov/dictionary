//
//  WordDefinition.swift
//  WordCatalogKit
//
//  Created by iBol on 8/9/21.
//

import Foundation

public struct WordDefinition: Decodable {
	public let text: String?
	public let soundUrl: String?
}
