# FDNetworkLayer

[![CI Status](https://img.shields.io/travis/iBol/FDNetworkLayer.svg?style=flat)](https://travis-ci.org/iBol/FDNetworkLayer)
[![Version](https://img.shields.io/cocoapods/v/FDNetworkLayer.svg?style=flat)](https://cocoapods.org/pods/FDNetworkLayer)
[![License](https://img.shields.io/cocoapods/l/FDNetworkLayer.svg?style=flat)](https://cocoapods.org/pods/FDNetworkLayer)
[![Platform](https://img.shields.io/cocoapods/p/FDNetworkLayer.svg?style=flat)](https://cocoapods.org/pods/FDNetworkLayer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FDNetworkLayer is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FDNetworkLayer'
```

## Author

iBol, aibolseed@gmail.com

## License

FDNetworkLayer is available under the MIT license. See the LICENSE file for more info.
