	//
//  AlamofireDebuggerLogger.swift
//  AlabsNetwork
//
//  Created by Ramazan on 12/7/20.
//

import Alamofire

final class AlamofireDebuggerLogger: EventMonitor {
	struct Constants {
		static let RequestHandledKey = "URLProtocolRequestHandled"
	}
	
	static var ignoredHosts = [String]()
	private var currentObjectType: Any?
	private var currentRequest: RequestModel?
	weak var session: Session?
    
    init(isDebugMenuShakeEnabled: Bool, debugMenuIgnoredHosts: [String]) {
        DebugMenu.shakeEnabled = isDebugMenuShakeEnabled
        Self.ignoredHosts = debugMenuIgnoredHosts
    }
	
	deinit {
		currentRequest = nil
	}
	
	private func body(from request: URLRequest) -> Data? {
		return request.httpBody
	}
	
	/// Inspects the request to see if the host has not been blacklisted and can be handled by this URL protocol.
	/// - Parameter request: The request being processed.csccsv
	private class func shouldHandleRequest(_ request: URLRequest) -> Bool {
		guard let host = request.url?.host, DebugMenu.isEnabled else { return false }

		return AlamofireDebuggerLogger.ignoredHosts.filter({ host.hasSuffix($0) }).isEmpty
	}
	
	func request(_ request: Request, didCompleteTask task: URLSessionTask, with error: AFError?) {
		let data = (request as? DataRequest)?.data
		guard let newRequest = task.originalRequest else { return }
		currentRequest = RequestModel(request: newRequest, session: session?.session)
		guard let currentRequest = currentRequest else { return }
		currentRequest.dataResponse = data
		currentRequest.httpBody = newRequest.httpBody
		currentRequest.duration = request.metrics?.taskInterval.duration
		Storage.shared.saveRequest(request: currentRequest)
	}
	
	func request<Value>(_ request: DataRequest, didParseResponse response: DataResponse<Value, AFError>) {
		setResponseStatus(error: response.error)
		if let response = response.response {
			currentRequest?.initResponse(response: response)
		}
		Storage.shared.saveRequest(request: currentRequest)
	}
	
	func request(_ request: DataRequest, didParseResponse response: DataResponse<Data?, AFError>) {
		setResponseStatus(error: response.error)
		if let response = response.response {
			currentRequest?.initResponse(response: response)
		}
		Storage.shared.saveRequest(request: currentRequest)
	}
	
	private func setResponseStatus(error: AFError?) {
		if let error = error {
			if case let .responseSerializationFailed(reason) = error,
			   case let .decodingFailed(error) = reason {
				currentRequest?.responseStatus.status = .failure(error: error)
			}
		} else {
			currentRequest?.responseStatus.status = .success
		}
	}
	
	func request(_ request: DataRequest, didValidateRequest urlRequest: URLRequest?, response: HTTPURLResponse, data: Data?, withResult result: Request.ValidationResult) {
		currentRequest?.initResponse(response: response)
		Storage.shared.saveRequest(request: currentRequest)
	}
}
