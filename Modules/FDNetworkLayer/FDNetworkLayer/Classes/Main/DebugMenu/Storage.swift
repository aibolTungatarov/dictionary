//
//  Storage.swift
//  AlabsNetwork
//
//  Created by Ramazan on 12/4/20.
//

import Foundation

class Storage {
	static let shared = Storage()
	static var limit: NSNumber? = nil
	var requests: [RequestModel] = []
	
	func saveRequest(request: RequestModel?) {
		guard let request = request else { return }
		if let index = requests.firstIndex(where: { request.id == $0.id }) {
			requests[index] = request
		} else {
			requests.insert(request, at: 0)
		}
		if let limit = Self.limit?.intValue {
			requests = Array(requests.prefix(limit))
		}
	}
	
	func clearRequests() {
		requests.removeAll()
	}
}
