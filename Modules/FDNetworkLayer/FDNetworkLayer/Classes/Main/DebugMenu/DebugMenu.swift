//
//  DebugMenu.swift
//  AlabsNetwork
//
//  Created by Ramazan on 12/8/20.
//

import Foundation
import UIKit

public class DebugMenu: NSObject
{
	public static var isEnabled: Bool = true
	@available(*, deprecated, renamed: "ignoredHosts")
	@objc public static var blacklistedHosts: [String] {
		get { return AlamofireDebuggerLogger.ignoredHosts }
		set { AlamofireDebuggerLogger.ignoredHosts = newValue }
	}

	/// Hosts that will be ignored from being recorded
	///
	@objc public static var ignoredHosts: [String] {
		get { return AlamofireDebuggerLogger.ignoredHosts }
		set { AlamofireDebuggerLogger.ignoredHosts = newValue }
	}
  
	/// Limit the logging count
	///
	@objc public static var limit: NSNumber? {
		get { Storage.limit }
		set { Storage.limit = newValue }
	}
	
	@objc public static func swiftyLoad() {
		NotificationCenter.default.addObserver(forName: fireDebugMenuNotification, object: nil, queue: nil) { (notification) in
			DebugMenu.presentDebugMenuFlow()
		}
	}
	
	@objc public static func swiftyInitialize() {
		if self == DebugMenu.self {
			DebugMenu.isEnabled = true
		}
	}
	
	// MARK: - Navigation
	static public func presentDebugMenuFlow(){
		guard UIViewController.currentViewController()?.isKind(of: WHBaseViewController.classForCoder()) == false && UIViewController.currentViewController()?.isKind(of: WHNavigationController.classForCoder()) == false else {
			return
		}
		let storyboard = UIStoryboard(name: "Flow", bundle: WHBundle.getBundle())
		if let initialVC = storyboard.instantiateInitialViewController(){
			initialVC.modalPresentationStyle = .fullScreen
			UIViewController.currentViewController()?.present(initialVC, animated: true, completion: nil)
		}
	}

	@objc public static var debugMenuFlow: UIViewController? {
		let storyboard = UIStoryboard(name: "Flow", bundle: WHBundle.getBundle())
		return storyboard.instantiateInitialViewController()
	}
	
	@objc public static var shakeEnabled: Bool = {
		let key = "DEBUG_MENU_SHAKE_ENABLED"
		
		if let environmentVariable = ProcessInfo.processInfo.environment[key] {
			return environmentVariable != "NO"
		}
		
		let arguments = UserDefaults.standard.volatileDomain(forName: UserDefaults.argumentDomain)
		if let arg = arguments[key] {
			switch arg {
			case let boolean as Bool: return boolean
			case let string as NSString: return string.boolValue
			case let number as NSNumber: return number.boolValue
			default: break
			}
		}
		
		return true
	}()
}

public let fireDebugMenuNotification = NSNotification.Name(rawValue: "debug_menu_fire")
public let newRequestNotification = NSNotification.Name(rawValue: "debug_menu_new_request")

extension DebugMenu: SelfAware {
	static func awake() {
		initializeAction
	}
	
	private static let initializeAction: Void = {
		swiftyLoad()
		swiftyInitialize()
	}()
}
