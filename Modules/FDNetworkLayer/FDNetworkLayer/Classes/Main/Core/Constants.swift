//
//  Constants.swift
//  AlabsNetwork
//
//  Created by Nurlan Tolegenov on 4/23/20.
//

import Foundation

enum Constants {
    static let authorization = "Authorization"
    static let defaultContentType = "application/x-www-form-urlencoded"
    static let acceptableStatusCodes = 200 ..< 300
    static let jsonDecoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
}
