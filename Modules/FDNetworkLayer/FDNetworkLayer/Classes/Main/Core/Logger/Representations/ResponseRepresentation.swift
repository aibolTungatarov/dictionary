//
//  ResponseRepresentation.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/5/20.
//

import Foundation

final class ResponseRepresentation: NSObject {
    let type: RepresentationType
    let statusCode: Int
    let urlString: String
    let headers: [String: String]
    let body: Data?
    let deserializedBody: String?
    let duration: TimeInterval?

    var statusString: String {
        HTTPURLResponse.localizedString(forStatusCode: statusCode)
    }

    var contentType: String {
        headers["Content-Type"] ?? Constants.defaultContentType
    }

    init(type: RepresentationType,
         statusCode: Int,
         urlString: String,
         headers: [String: String],
         body: Data?,
         deserializedBody: String?,
         duration: TimeInterval?) {
        self.type = type
        self.statusCode = statusCode
        self.urlString = urlString
        self.headers = headers
        self.body = body
        self.deserializedBody = deserializedBody
        self.duration = duration
    }

    convenience init(type: RepresentationType, response: HTTPURLResponse, body: Data?, deserializedBody: String?, duration: TimeInterval?) {
        self.init(type: type,
                  statusCode: response.statusCode,
                  urlString: response.url?.absoluteString ?? "",
                  headers: response.allHeaderFields as? [String: String] ?? [:],
                  body: body,
                  deserializedBody: deserializedBody,
                  duration: duration)
    }
}
