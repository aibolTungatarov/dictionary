//
//  RequestRepresentation.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/5/20.
//

import Foundation

final class RequestRepresentation: NSObject {
    let type: RepresentationType
    let method: String
    let urlString: String
	let headers: [String: String]
    let body: Data?
    let deserializedBody: String?
    let retryCount: Int?
    var contentType: String {
        headers["Content-Type"] ?? Constants.defaultContentType
    }

    init(type: RepresentationType = .request,
         method: String,
         urlString: String,
         headers: [String: String],
         body: Data?,
         deserializedBody: String?,
         retryCount: Int?) {
        self.type = type
        self.method = method
        self.urlString = urlString
		self.headers = headers.isEmpty ? ["Content-Type": Constants.defaultContentType] : headers
        self.body = body
        self.deserializedBody = deserializedBody
        self.retryCount = retryCount
    }

    convenience init(type: RepresentationType = .request, request: URLRequest, deserializedBody: String?, retryCount: Int?) {
        self.init(type: type,
                  method: request.httpMethod ?? "GET",
                  urlString: request.url?.absoluteString ?? "",
                  headers: request.allHTTPHeaderFields ?? [:],
                  body: request.httpBody,
                  deserializedBody: deserializedBody,
                  retryCount: retryCount)
    }
}
