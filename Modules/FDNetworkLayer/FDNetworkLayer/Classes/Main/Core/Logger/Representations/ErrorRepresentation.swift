//
//  ErrorRepresentation.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/5/20.
//

import Foundation

final class ErrorRepresentation: NSObject {
    let type: RepresentationType
    let response: ResponseRepresentation?
    let domain: String
    let code: Int
    let reason: String
    let userInfo: [String: Any]

    init(type: RepresentationType = .error,
         response: ResponseRepresentation?,
         domain: String,
         code: Int,
         reason: String,
         userInfo: [String: Any]) {
        self.type = type
        self.response = response
        self.domain = domain
        self.code = code
        self.reason = reason
        self.userInfo = userInfo
    }

    convenience init(type: RepresentationType = .error, error: NSError, response: ResponseRepresentation?) {
        self.init(type: type,
                  response: response,
                  domain: error.domain,
                  code: error.code,
                  reason: error.localizedDescription,
                  userInfo: error.userInfo)
    }
}
