//
//  RepresentationType.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/6/20.
//

import Foundation

enum RepresentationType {
    case request
    case response
    case retry
    case error
    case warning
}

extension RepresentationType {
    var symbol: String {
        switch self {
        case .request, .response:
            return "✅"
        case .retry:
            return "🔄"
        case .error:
            return "⛔️"
        case .warning:
            return "⚠️"
        }
    }

    var title: String {
        switch self {
        case .request:
            return "[REQUEST]"
        case .response:
            return "[RESPONSE]"
        case .retry:
            return "[RETRY]"
        case .error:
            return "[ERROR]"
        case .warning:
            return "[WARNING RESPONSE]"
        }
    }
}
