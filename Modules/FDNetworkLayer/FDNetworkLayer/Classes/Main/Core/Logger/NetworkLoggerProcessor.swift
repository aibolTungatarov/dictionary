//
//  NetworkLoggerProcessor.swift
//  AlabsFoundation
//
//  Created by Aidar Nugmanov on 5/15/20.
//

import Foundation

final class NetworkLoggerProcessor {
    static func process(request: URLRequest, retryCount: Int? = nil) {
        let type: RepresentationType = retryCount != nil ? .retry : .request
        let deserializedBody = standardizedData(of: request).flatMap { data in
            CompositeDeserializer.deserialize(body: data,
                                              contentType: request.value(forHTTPHeaderField: "Content-Type") ??
                                                  Constants.defaultContentType)
        }

        let requestRepresentation = RequestRepresentation(type: type, request: request, deserializedBody: deserializedBody, retryCount: retryCount)
        ConsoleOutputFacility.output(requestRepresentation: requestRepresentation)
    }

    static func process(response: HTTPURLResponse, data: Data?, duration: TimeInterval?) {
        let isAcceptable = Constants.acceptableStatusCodes.contains(response.statusCode)
        let type: RepresentationType = isAcceptable ? .response : .warning
        let deserializedBody = data.flatMap { data in
            CompositeDeserializer.deserialize(body: data, contentType: response.allHeaderFields["Content-Type"] as? String)
        }
        let responseRepresentation = ResponseRepresentation(type: type,
                                                            response: response,
                                                            body: data,
                                                            deserializedBody: deserializedBody,
                                                            duration: duration)
        ConsoleOutputFacility.output(responseRepresentation: responseRepresentation)
    }

    static func process(error: NSError, response: HTTPURLResponse?, data: Data?, duration: TimeInterval?) {
        let deserializedBody = response.flatMap { response in
            data.flatMap { data in
                CompositeDeserializer.deserialize(body: data, contentType: response.allHeaderFields["Content-Type"] as? String)
            }
        }
        let responseRepresentation = response.flatMap { response -> ResponseRepresentation in
            let isAcceptable = Constants.acceptableStatusCodes.contains(response.statusCode)
            let type: RepresentationType = isAcceptable ? .response : .warning
            return ResponseRepresentation(type: type, response: response, body: data, deserializedBody: deserializedBody, duration: duration)
        }
        let errorRepresentation = ErrorRepresentation(error: error, response: responseRepresentation)
        ConsoleOutputFacility.output(errorRepresentation: errorRepresentation)
    }

    private static func standardizedData(of request: URLRequest) -> Data? {
        request.httpBody ?? request.httpBodyStream.flatMap { stream in
            let data = NSMutableData()
            stream.open()
            while stream.hasBytesAvailable {
                var buffer = [UInt8](repeating: 0, count: 1024)
                let length = stream.read(&buffer, maxLength: buffer.count)
                data.append(buffer, length: length)
            }
            stream.close()
            return data as Data
        }
    }
}
