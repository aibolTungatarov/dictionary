//
//  ConsoleOutputFacility.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/5/20.
//

import Foundation

final class ConsoleOutputFacility: NSObject {
    /// Распечатывает запрос в следующем формате:
    ///
    ///     <✅> [REQUEST] POST https://httpbin.org/post
    ///      ├─ Headers
    ///      │ Content-Type: application/json
    ///      │ Content-Length: 14
    ///      ├─ Body
    ///      │ {
    ///      │   "foo": "bar"
    ///      │ }
    ///

    static func output(requestRepresentation request: RequestRepresentation) {
        let headers = request.headers.reduce([]) {
            $0 + ["\($1.0): \($1.1)"]
        }
        let body = request.deserializedBody.map {
            $0.split { $0 == "\n" }.map(String.init)
        } ?? ["<none>"]
        printBoxString(title: "<\(request.type.symbol)> \(request.type.title) \(request.method) \(request.urlString)", sections: [
            ("Headers", headers),
            ("Body", body)
        ])
    }

    /// Распечатывает ответ в следующем формате:
    ///
    ///     <✅> [RESPONSE] 200 (NO ERROR) https://httpbin.org/post
    ///      ├─ Headers
    ///      │ Content-Type: application/json
    ///      │ Content-Length: 24
    ///      ├─ Body
    ///      │ {
    ///      │   "args": {},
    ///      │   "headers": {}
    ///      │ }
    ///

    static func output(responseRepresentation response: ResponseRepresentation) {
        let headers = response.headers.reduce([]) {
            $0 + ["\($1.0): \($1.1)"]
        }
        let body = response.deserializedBody.map {
            $0.split { $0 == "\n" }.map(String.init)
        } ?? ["<none>"]
        let duration = formatDuration(duration: response.duration)
        let title = """
        <\(response.type.symbol)>
        \(response.type.title) \(response.statusCode)
        (\(response.statusString.uppercased())) \(response.urlString)
        """
        printBoxString(title: title, sections: [
            ("Duration", [duration]),
            ("Headers", headers),
            ("Body", body)
        ])
    }

    /// Распечатывает ошибку в следующем формате:
    ///
    ///     <⛔️> [ERROR] NSURLErrorDomain -1009
    ///      ├─ User Info
    ///      │ NSLocalizedDescriptionKey: The device is not connected to the internet.
    ///      │ NSURLErrorKey: https://httpbin.org/post
    ///

    static func output(errorRepresentation error: ErrorRepresentation) {
        let response = error.response
        let userInfo = error.userInfo.reduce([]) {
            $0 + ["\($1.0): \($1.1)"]
        }
        let duration = formatDuration(duration: response?.duration)
        printBoxString(title: "<\(error.type.symbol)> \(error.type.title) \(error.domain) \(error.code)", sections: [
            ("Duration", [duration]),
            ("User Info", userInfo),
            ("Reason", [error.reason])
        ])
    }

    /// Создаёт строку в формате коробки:
    ///
    ///     box title
    ///      ├─ section title
    ///      │ section
    ///      │ contents
    ///

    private static func composeBoxString(title: String, sections: [(String, [String])]) -> String {
        "\(title)\n" + sections.reduce("") { accumulator, sections in
            let (sectionTitle, elements) = sections
            var delimiter = "•"
            if sectionTitle == "Body" {
                delimiter = ""
            } else if sectionTitle == "Duration" {
                delimiter = "⏱"
            }
            return "\(accumulator) ├─ \(sectionTitle)\n" + elements.reduce("") {
                "\($0) │ \(delimiter) \($1)\n"
            }
        }
    }

    private static func printBoxString(title: String, sections: [(String, [String])]) {
        print(composeBoxString(title: title, sections: sections))
    }

    private static func formatDuration(duration: TimeInterval?) -> String {
        if let duration = duration {
            return String(format: "%.03f", duration) + "s"
        } else {
            return "<unknown>"
        }
    }
}
