//
//  CompositeDeserializer.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/5/20.
//

import Foundation

private extension Constants {
    static let bodyDeserializers: [String: BodyDeserializer] = [
        "*/json": JSONBodyDeserializer(),
        "image/*": ImageBodyDeserializer()
    ]
}

final class CompositeDeserializer {
    static func deserialize(body: Data, contentType: String?) -> String? {
        let contentType = contentType ?? Constants.defaultContentType
        guard let deserializer = findBodyDeserializer(forContentType: contentType) else { return nil }
        return deserializer.deserialize(body: body)
    }

    private static func findBodyDeserializer(forContentType contentType: String) -> BodyDeserializer? {
        guard let trimmedContentType = contentType.components(separatedBy: ";").first?.trimmingCharacters(in: .whitespaces) else { return nil }
        for (pattern, deserializer) in Constants.bodyDeserializers {
            let patternParts = pattern.components(separatedBy: "/")
            let actualParts = trimmedContentType.components(separatedBy: "/")
            guard patternParts.count == 2, actualParts.count == 2 else { return nil }
            if ["*", actualParts[0]].contains(patternParts[0]), ["*", actualParts[1]].contains(patternParts[1]) {
                return deserializer
            }
        }
        return nil
    }
}
