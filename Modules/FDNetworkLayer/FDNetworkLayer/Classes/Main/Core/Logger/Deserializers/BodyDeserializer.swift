//
//  BodyDeserializer.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/6/20.
//

import Foundation

protocol BodyDeserializer {
    func deserialize(body: Data) -> String?
}
