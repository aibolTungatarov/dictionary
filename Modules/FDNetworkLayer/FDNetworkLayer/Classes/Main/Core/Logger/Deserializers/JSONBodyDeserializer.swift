//
//  JSONBodyDeserializer.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/5/20.
//

import Foundation

final class JSONBodyDeserializer: NSObject, BodyDeserializer {
    func deserialize(body: Data) -> String? {
        do {
            let object = try JSONSerialization.jsonObject(with: body, options: [])
            let data = try JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted])
            return String(data: data, encoding: .utf8)
        } catch {
            return nil
        }
    }
}
