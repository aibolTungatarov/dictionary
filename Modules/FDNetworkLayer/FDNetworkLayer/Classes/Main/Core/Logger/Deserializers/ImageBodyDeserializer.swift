//
//  ImageBodyDeserializer.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 3/5/20.
//

import UIKit

final class ImageBodyDeserializer: NSObject, BodyDeserializer {
    private typealias Image = UIImage

    func deserialize(body: Data) -> String? {
        Image(data: body).map { "\(Int($0.size.width))px × \(Int($0.size.height))px image" }
    }
}
