//
//  NetworkLoggerLevel.swift
//  AlabsFoundation
//
//  Created by Aidar Nugmanov on 5/15/20.
//

import Foundation

public enum NetworkLoggerLevel {
    /// Логирует все запросы и ответы.
    case debug
    /// Отключает логгер.
    case off
}
