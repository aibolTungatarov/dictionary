//
//  NetworkParametersStorable.swift
//  AlabsNetworkExample
//
//  Created by Nurbek Ismagulov on 9/27/20.
//  Copyright © 2020 Azimut Labs. All rights reserved.
//

import Alamofire

public protocol NetworkParametersStorable {
    var parameters: Parameters { get }
}
