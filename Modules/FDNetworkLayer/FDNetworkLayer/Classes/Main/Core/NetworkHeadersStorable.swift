//
//  NetworkHeadersStorage.swift
//  AlabsNetworkExample
//
//  Created by Aidar Nugmanoff on 9/8/20.
//  Copyright © 2020 Azimut Labs. All rights reserved.
//

import Foundation

public typealias NetworkHeaders = [String: String]

public protocol NetworkHeadersStorable {
    var defaultHeaders: NetworkHeaders { get }
}
