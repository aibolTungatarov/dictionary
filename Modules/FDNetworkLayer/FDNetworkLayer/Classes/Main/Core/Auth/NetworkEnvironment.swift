//
//  NetworkEnvironment.swift
//  FDNetworkLayer
//
//  Created by Ramazan Kazybek on 5/13/21.
//

public struct NetworkEnvironment: NetworkHeadersStorable {
	public var defaultHeaders: NetworkHeaders {
		return [:]
	}
}
