//
//  NetworkAuthCredentials.swift
//  AlabsNetwork
//
//  Created by Nurlan Tolegenov on 6/17/20.
//

import Foundation
import FDFoundation

public struct NetworkAuthCredentials: Decodable {
    enum CodingKeys: String, CodingKey {
        case accessToken = "access"
        case refreshToken = "refresh"
    }

    public let accessToken: String
    public let refreshToken: String
}

public protocol NetworkAuthCredentialsStorable: class {
    var accessToken: String? { get }
    var refreshToken: String? { get }
    func save(credentials: NetworkAuthCredentials)
}

extension UserSessionStorage: NetworkAuthCredentialsStorable {
	public func save(credentials: NetworkAuthCredentials) {
		accessToken = credentials.accessToken
		refreshToken = credentials.refreshToken
	}
}
