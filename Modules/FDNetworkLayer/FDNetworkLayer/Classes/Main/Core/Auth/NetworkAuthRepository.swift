//
//  NetworkAuthRepository.swift
//  AlabsNetwork
//
//  Created by Nurlan Tolegenov on 6/17/20.
//

import Alamofire

public protocol NetworkAuthStateObservable: class {
    func tokenDidExpire()
}

public protocol NetworkAuthProviding: class {
    func postToken(_ refreshToken: String) -> DataRequest
}

final class NetworkAuthRepository {
    weak var provider: NetworkAuthProviding?
    var accessToken: String? {
        storage.accessToken
    }

    private let storage: NetworkAuthCredentialsStorable
    private let observer: NetworkAuthStateObservable

    init(storage: NetworkAuthCredentialsStorable, observer: NetworkAuthStateObservable) {
        self.storage = storage
        self.observer = observer
    }

    func makeRefreshTokenRequest() -> DataRequest? {
        guard let provider = provider,
            let refreshToken = storage.refreshToken else { return nil }
        return provider.postToken(refreshToken)
    }

    func handleRefreshTokenRequest(_ request: DataRequest, completion: @escaping (Result<Void, Error>) -> Void) {
        request.responseDecodable { [weak self] (response: DataResponse<NetworkAuthCredentials, AFError>) in
            guard let self = self else { return }
            switch response.result {
            case let .success(response):
                self.storage.save(credentials: response)
                completion(.success(()))
            case let .failure(error):
                completion(.failure(error))
                if response.response?.statusCode == 401 {
                    self.observer.tokenDidExpire()
                }
            }
        }
    }
}
