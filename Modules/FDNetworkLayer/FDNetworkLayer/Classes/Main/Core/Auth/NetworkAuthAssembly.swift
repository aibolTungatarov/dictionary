//
//  NetworkAuthAssembly.swift
//  FDNetworkLayer
//
//  Created by Ramazan Kazybek on 5/13/21.
//

import EasyDi
import FDFoundation

final public class NetworkAuthAssembly: Assembly {
	public var service: NetworkAuth {
		let storage = UserSessionStorage()
		return define(scope: Scope.lazySingleton, init: NetworkAuth(storage: storage,
																	observer: AuthStateObserver(userSession: UserSession(storage: storage))))
	}
}
