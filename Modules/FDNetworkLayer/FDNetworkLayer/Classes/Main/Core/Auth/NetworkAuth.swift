//
//  NetworkAuth.swift
//  AlabsNetwork
//
//  Created by Nurlan Tolegenov on 6/17/20.
//

import Alamofire

public final class NetworkAuth {
    let repository: NetworkAuthRepository
    var pendingRequest: DataRequest?

    public init(storage: NetworkAuthCredentialsStorable, observer: NetworkAuthStateObservable) {
        repository = NetworkAuthRepository(storage: storage, observer: observer)
    }

    func handleTokenExpired(completion: @escaping (RetryResult) -> Void) {
        guard pendingRequest == nil else { return }
        guard let request = refreshToken() else {
            completion(.doNotRetry)
            handleRequestsToRetry(.doNotRetry)
            return
        }
        repository.handleRefreshTokenRequest(request) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                self.handleRequestsToRetry(.retry)
            case let .failure(error):
                self.handleRequestsToRetry(.doNotRetryWithError(error))
            }
        }
        pendingRequest = request
    }

    private func refreshToken() -> DataRequest? {
        if let request = pendingRequest {
            return request
        }
        return repository.makeRefreshTokenRequest()
    }

    private func handleRequestsToRetry(_ shouldRetry: RetryResult) {
        pendingRequest = nil
    }
}
