//
//  NetworkInterceptor.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 4/22/20.
//

import Alamofire

final class AlamofireNetworkInterceptor: Interceptor {
    enum RetryReason {
        case limitExceeded
        case tokenExpired
        case networkFailed
    }

    private var auth: NetworkAuth?
    private let headersStorage: NetworkHeadersStorable

    init(auth: NetworkAuth, headersStorage: NetworkHeadersStorable) {
        self.auth = auth
        self.headersStorage = headersStorage
        super.init()
    }
    
    init(headersStorage: NetworkHeadersStorable) {
        self.headersStorage = headersStorage
        super.init()
    }

    override func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        var adaptedUrlRequest = urlRequest
        adaptedUrlRequest.headers.add(contentsOf: HTTPHeaders(headersStorage.defaultHeaders))
        adaptedUrlRequest.headers.add(.defaultUserAgent)
        if let auth = auth, let token = auth.repository.accessToken, urlRequest.headers.value(for: Constants.authorization) == nil {
            adaptedUrlRequest.headers.add(.authorization(bearerToken: token))
        }
        completion(.success(adaptedUrlRequest))
    }

    override func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard let auth = auth else {
            completion(.doNotRetry)
            return
        }
        let reason = retryReason(for: request, dueTo: error)
        switch reason {
        case .networkFailed:
            completion(.retry)
        case .limitExceeded, .none:
            completion(.doNotRetry)
        case .tokenExpired:
            auth.handleTokenExpired(completion: completion)
        }
    }

    private func retryReason(for request: Request, dueTo error: Error) -> RetryReason? {
        guard let auth = auth else {
            return .none
        }
        if request.retryCount > RetryPolicy.defaultRetryLimit {
            return .limitExceeded
        } else if let response = request.response, response.statusCode == 401 {
            return request == auth.pendingRequest ? .none : .tokenExpired
        } else if let errorCode = (error as? URLError)?.code, RetryPolicy.defaultRetryableURLErrorCodes.contains(errorCode) {
            return .networkFailed
        } else {
            return .none
        }
    }
}
