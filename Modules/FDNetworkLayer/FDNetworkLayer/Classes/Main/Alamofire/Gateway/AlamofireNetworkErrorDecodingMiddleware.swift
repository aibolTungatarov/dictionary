//
//  AlamofireNetworkErrorDecodingMiddleware.swift
//  AlabsNetwork
//
//  Created by Nurlan Tolegenov on 4/22/20.
//

import Alamofire

public typealias ApiError = Decodable & Error

public final class AlamofireNetworkErrorDecodingMiddleware<Error: ApiError> {
    public func process(request: URLRequest?, response: HTTPURLResponse, data: Data?) -> Result<Void, Swift.Error> {
        if Constants.acceptableStatusCodes.contains(response.statusCode) {
            return .success(())
        } else {
            guard let data = data else {
                let reason: AFError.ResponseValidationFailureReason
                if let url = request?.url {
                    reason = .dataFileReadFailed(at: url)
                } else {
                    reason = .dataFileNil
                }
                return .failure(AFError.responseValidationFailed(reason: reason))
            }
            do {
                let error = try JSONDecoder().decode(Error.self, from: data)
                return .failure(error)
            } catch {
                return .failure(error)
            }
        }
    }
}
