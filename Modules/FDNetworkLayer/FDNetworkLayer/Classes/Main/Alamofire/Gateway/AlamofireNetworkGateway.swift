//
//  AlamofireNetworkGateway.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 4/22/20.
//

import Alamofire

final class AlamofireNetworkGateway<Error: ApiError> {
    private let middleware: AlamofireNetworkErrorDecodingMiddleware<Error>
    private let session: Session
	private let debuggerLogger: AlamofireDebuggerLogger
	
    init(middleware: AlamofireNetworkErrorDecodingMiddleware<Error>,
         auth: NetworkAuth,
         headersStorage: NetworkHeadersStorable,
         loggerLevel: NetworkLoggerLevel,
         isDebugMenuShakeEnabled: Bool,
         debugMenuIgnoredHosts: [String]) {
        self.middleware = middleware
        debuggerLogger = AlamofireDebuggerLogger(isDebugMenuShakeEnabled: isDebugMenuShakeEnabled, debugMenuIgnoredHosts: debugMenuIgnoredHosts)
        session = Session(interceptor: AlamofireNetworkInterceptor(auth: auth, headersStorage: headersStorage),
                          eventMonitors: [AlamofireNetworkLogger(level: loggerLevel),
										  debuggerLogger])
		debuggerLogger.session = session
    }
    
    init(middleware: AlamofireNetworkErrorDecodingMiddleware<Error>,
         headersStorage: NetworkHeadersStorable,
         loggerLevel: NetworkLoggerLevel,
         isDebugMenuShakeEnabled: Bool,
         debugMenuIgnoredHosts: [String]) {
        self.middleware = middleware
        debuggerLogger = AlamofireDebuggerLogger(isDebugMenuShakeEnabled: isDebugMenuShakeEnabled, debugMenuIgnoredHosts: debugMenuIgnoredHosts)
        session = Session(interceptor: AlamofireNetworkInterceptor(headersStorage: headersStorage),
                          eventMonitors: [AlamofireNetworkLogger(level: loggerLevel),
                                          debuggerLogger])
        debuggerLogger.session = session
    }
    
    func request(_ converter: URLRequestConvertible) -> DataRequest {
        let request = session.request(converter)
        let result = request.validate(middleware.process(request:response:data:))
        return result
    }

    func request<Parameters: Encodable>(_ url: String,
                                        method: HTTPMethod,
                                        parameters: Parameters? = nil,
                                        encoder: ParameterEncoder,
                                        headers: HTTPHeaders? = nil) -> DataRequest {
        
        let request = session.request(url, method: method, parameters: parameters, encoder: encoder, headers: headers)
        let result = request.validate(middleware.process(request:response:data:))
        return result
    }

    func request(_ url: String,
                 method: HTTPMethod,
                 parameters: Parameters? = nil,
                 encoding: ParameterEncoding,
                 headers: HTTPHeaders? = nil) -> DataRequest {
        let request = session.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        let result = request.validate(middleware.process(request:response:data:))
        return result
    }
}
