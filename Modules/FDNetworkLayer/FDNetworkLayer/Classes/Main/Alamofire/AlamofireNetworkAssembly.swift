//
//  AlamofireNetworkAssembly.swift
//  Alamofire
//
//  Created by Ramazan Kazybek on 5/13/21.
//

import EasyDi

final public class AlamofireNetworkAssembly: Assembly {
	private lazy var auth: NetworkAuthAssembly = DIContext.defaultInstance.assembly()
	
	public var service: AlamofireNetwork<ProviderError> {
		return define(scope: Scope.lazySingleton,
					  init: AlamofireNetwork<ProviderError>(baseUrl: "https://dictionary.skyeng.ru",
															auth: self.auth.service,
															headersStorage: NetworkEnvironment(),
															loggerLevel: .debug,
															isDebugMenuShakeEnabled: true,
															debugMenuIgnoredHosts: []))
	}
}
