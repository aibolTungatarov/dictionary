//
//  AlamofireNetworkLogger.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 2/24/20.
//

import Alamofire

final class AlamofireNetworkLogger: EventMonitor {
    private let level: NetworkLoggerLevel

    init(level: NetworkLoggerLevel) {
        self.level = level
    }

    func request(_ request: Request, didCompleteTask task: URLSessionTask, with error: AFError?) {
        guard level == .debug else { return }
        let data = (request as? DataRequest)?.data
        let duration = request.metrics?.taskInterval.duration
        let response = request.response
        if let error = error {
            NetworkLoggerProcessor.process(error: error as NSError, response: response, data: data, duration: duration)
        } else if let response = response {
            NetworkLoggerProcessor.process(response: response, data: data, duration: duration)
        }
    }

    func request(_ request: Request, didResumeTask task: URLSessionTask) {
        guard level == .debug,
            let request = task.originalRequest else { return }
        NetworkLoggerProcessor.process(request: request)
    }

    func requestIsRetrying(_ request: Request) {
        guard level == .debug,
            let urlRequest = request.task?.originalRequest else { return }
        NetworkLoggerProcessor.process(request: urlRequest, retryCount: request.retryCount)
    }
}
