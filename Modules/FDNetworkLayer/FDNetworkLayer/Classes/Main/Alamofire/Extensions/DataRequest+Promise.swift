//
//  DataRequest+Promise.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 4/22/20.
//

import Alamofire
import Promises

extension DataRequest {
    @discardableResult
    func toPromise<Response: Decodable>() -> Promise<Response> {
        wrap { completionHandler in
            self.responseDecodable(decoder: Constants.jsonDecoder, completionHandler: completionHandler)
        }.then { response -> Response in
            try response.result.get()
        }
    }
    
    @discardableResult
    func toPromise() -> Promise<Data> {
        wrap { completionHandler in
            self.responseData(completionHandler: completionHandler)
        }.then { response -> Data in
            try response.result.get()
        }
    }
}
