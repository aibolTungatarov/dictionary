//
//  HTTPHeaders+Add.swift
//  AlabsNetworkExample
//
//  Created by Aidar Nugmanoff on 9/8/20.
//  Copyright © 2020 Azimut Labs. All rights reserved.
//

import Alamofire

extension HTTPHeaders {
    mutating func add(contentsOf headers: HTTPHeaders) {
        for header in headers {
            add(header)
        }
    }
}
