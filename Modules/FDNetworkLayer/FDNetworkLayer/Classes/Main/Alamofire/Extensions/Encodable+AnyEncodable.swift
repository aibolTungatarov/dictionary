//
//  Encodable+AnyEncodable.swift
//  AlabsNetworkExample
//
//  Created by Nurbek Ismagulov on 10/2/20.
//  Copyright © 2020 Azimut Labs. All rights reserved.
//

import Foundation

// http://yourfriendlyioscoder.com/blog/2019/04/27/any-encodable
struct AnyEncodable: Encodable {
    let value: Encodable

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try value.encode(to: &container)
    }
}

extension Encodable {
    func encode(to container: inout SingleValueEncodingContainer) throws {
        try container.encode(self)
    }
}

// https://github.com/satishVekariya/SVCodable
public extension Encodable {
    func toJSONData() -> Data? {
        return try? JSONEncoder().encode(self)
    }
    
    func toJSONString() -> String? {
        if let bytes = toJSONData() {
            return String(bytes: bytes, encoding: .utf8)
        }
        return nil
    }
    
    /// Конвертация Encodable объекта в Dictionary
    func toJSON() -> [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}
