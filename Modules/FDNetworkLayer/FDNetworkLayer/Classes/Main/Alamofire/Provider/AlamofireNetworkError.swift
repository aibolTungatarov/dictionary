//
//  AlamofireNetworkError.swift
//  AlabsNetwork
//
//  Created by Nurlan Tolegenov on 4/22/20.
//

import Foundation

open class AlamofireNetworkError: ApiError {
    public let message: String
    
	public init(message: String) {
        self.message = message
    }
}

extension AlamofireNetworkError: Equatable {
    public static func == (lhs: AlamofireNetworkError, rhs: AlamofireNetworkError) -> Bool {
        lhs.message == rhs.message
    }
}

extension AlamofireNetworkError: LocalizedError {
    public var errorDescription: String? {
        message
    }
}

// swiftlint:disable force_cast
public extension Error {
    var isInternetConnectionError: Bool {
        if let afError = asAFError, let urlError = afError.underlyingError as? URLError {
            return urlError.code == .notConnectedToInternet
        } else {
            return false
        }
    }
    
    var wrappedError: AlamofireNetworkError {
        wrapError(from: self) as! AlamofireNetworkError
    }

    private func wrapError(from error: Error) -> Error {
        if let apiError = error as? AlamofireNetworkError {
            return apiError
        }
        guard let afError = error.asAFError else {
            return AlamofireNetworkError(message: error.localizedDescription)
        }
        switch afError {
        case let .requestRetryFailed(retryError, _):
            return wrapError(from: retryError)
        default:
            guard let underlyingError = afError.underlyingError else {
                return AlamofireNetworkError(message: error.localizedDescription)
            }
            return wrapError(from: underlyingError)
        }
    }
}
