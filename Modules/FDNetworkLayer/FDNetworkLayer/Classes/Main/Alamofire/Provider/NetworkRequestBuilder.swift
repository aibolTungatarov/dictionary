//
//  NetworkRequestBuilder.swift
//  AlabsNetworkExample
//
//  Created by Nurbek Ismagulov on 9/28/20.
//  Copyright © 2020 Azimut Labs. All rights reserved.
//

import Alamofire

public class NetworkRequestBuilder {
    private var request: AlamofireNetworkRequest
    
    public init(with baseUrl: String,
                injectedParameters: NetworkParametersStorable? = nil,
                parametersInjectionPolicy: ParametersInjectionPolicy = .exclude) {
        self.request = AlamofireNetworkRequest(baseUrl: baseUrl,
                                      injectedParameters: injectedParameters,
                                      parametersInjectionPolicy: parametersInjectionPolicy)
    }
    
    @discardableResult
    public func set(method: HTTPMethod) -> Self {
        request.method = method
        return self
    }
    
    @discardableResult
    public func set(path: String) -> Self {
        request.path = path
        return self
    }

    @discardableResult
    public func set(headers: HTTPHeaders?) -> Self {
        request.headers = headers
        return self
    }
    
    @discardableResult
    public func set(parameters: Parameters) -> Self {
        request.parameters = parameters
        return self
    }
    
    @discardableResult
    public func set(parameters: Encodable) -> Self {
        request.parameters = parameters.toJSON()
        return self
    }
    
    @discardableResult
    public func set(parametersInjectionPolicy: ParametersInjectionPolicy) -> Self {
        request.parametersInjectionPolicy = parametersInjectionPolicy
        return self
    }
    
    public func build() -> AlamofireNetworkRequest {
        let changedRequest = request
        request.reset()
        return changedRequest
    }
}
