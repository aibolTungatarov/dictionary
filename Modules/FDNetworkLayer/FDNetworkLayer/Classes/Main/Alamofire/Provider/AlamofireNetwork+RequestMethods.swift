//
//  AlamofireNetwork+RequestMethods.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 4/22/20.
//

import Alamofire
import Promises

public extension AlamofireNetwork {
    func request(_ converter: URLRequestConvertible) -> Promise<Data> {
        request(with: converter)
    }
    
    func request<Response: Decodable>(_ converter: URLRequestConvertible) -> Promise<Response> {
        request(with: converter)
    }
    
    func get<Response>(_ url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> Promise<Response>
        where Response: Decodable {
        request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
    }

    func get<Parameters, Response>(_ url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> Promise<Response>
        where Parameters: Encodable, Response: Decodable {
        request(url, method: .get, parameters: parameters, encoder: URLEncodedFormParameterEncoder.default, headers: headers)
    }

    func put<Response>(_ url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> Promise<Response>
        where Response: Decodable {
        request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }

    func put<Parameters, Response>(_ url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> Promise<Response>
        where Parameters: Encodable, Response: Decodable {
        request(url, method: .put, parameters: parameters, encoder: JSONParameterEncoder.default, headers: headers)
    }

    func post<Response: Decodable>(_ url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> Promise<Response> {
        request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }
    
    func post(_ url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> DataRequest {
        request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }
    
    func post<Parameters, Response>(_ url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> Promise<Response>
        where Parameters: Encodable, Response: Decodable {
        request(url, method: .post, parameters: parameters, encoder: JSONParameterEncoder.default, headers: headers)
    }

    func delete<Response: Decodable>(_ url: String, parameters: Parameters? = nil, headers: HTTPHeaders? = nil) -> Promise<Response> {
        request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
    }
}
