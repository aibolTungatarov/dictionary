//
//  AlamofireNetwork.swift
//  AlabsNetwork
//
//  Created by Aidar Nugmanov on 4/22/20.
//

import Alamofire
import Promises

public final class AlamofireNetwork<Error: AlamofireNetworkError> {
    private let baseUrl: String
    private var auth: NetworkAuth?
    private let headersStorage: NetworkHeadersStorable
    private let loggerLevel: NetworkLoggerLevel
    private let isDebugMenuShakeEnabled: Bool
    private let debugMenuIgnoredHosts: [String]
    
    private lazy var gateway: AlamofireNetworkGateway<Error> = {
        let middleware = AlamofireNetworkErrorDecodingMiddleware<Error>()
        if let auth = auth {
            return AlamofireNetworkGateway<Error>(middleware: middleware,
                                                  auth: auth,
                                                  headersStorage: headersStorage,
                                                  loggerLevel: loggerLevel,
                                                  isDebugMenuShakeEnabled: isDebugMenuShakeEnabled,
                                                  debugMenuIgnoredHosts: debugMenuIgnoredHosts)
        } else {
            return AlamofireNetworkGateway<Error>(middleware: middleware,
                                                  headersStorage: headersStorage,
                                                  loggerLevel: loggerLevel,
                                                  isDebugMenuShakeEnabled: isDebugMenuShakeEnabled,
                                                  debugMenuIgnoredHosts: debugMenuIgnoredHosts)
        }
    }()

    public init(baseUrl: String,
                auth: NetworkAuth,
                headersStorage: NetworkHeadersStorable,
                loggerLevel: NetworkLoggerLevel,
                isDebugMenuShakeEnabled: Bool,
                debugMenuIgnoredHosts: [String]) {
        self.baseUrl = baseUrl
        self.auth = auth
        self.headersStorage = headersStorage
        self.loggerLevel = loggerLevel
        self.isDebugMenuShakeEnabled = isDebugMenuShakeEnabled
        self.debugMenuIgnoredHosts = debugMenuIgnoredHosts
    }
    
    public init(baseUrl: String,
                headersStorage: NetworkHeadersStorable,
                loggerLevel: NetworkLoggerLevel,
                isDebugMenuShakeEnabled: Bool,
                debugMenuIgnoredHosts: [String]) {
        self.baseUrl = baseUrl
        self.headersStorage = headersStorage
        self.loggerLevel = loggerLevel
        self.isDebugMenuShakeEnabled = isDebugMenuShakeEnabled
        self.debugMenuIgnoredHosts = debugMenuIgnoredHosts
    }

    public func setAuthProvider(_ provider: NetworkAuthProviding?) {
        auth?.repository.provider = provider
    }
    
    func request<Response: Decodable>(with converter: URLRequestConvertible) -> Promise<Response> {
        let request = gateway.request(converter)
        return request.toPromise()
    }
    
    func request(with converter: URLRequestConvertible) -> Promise<Data> {
        let request = gateway.request(converter)
        return request.toPromise()
    }
    
    func request(with converter: URLRequestConvertible) -> DataRequest {
        let request = gateway.request(converter)
        return request
    }

    func request<Response: Decodable, Parameters: Encodable>(_ url: String,
                                                             method: HTTPMethod,
                                                             parameters: Parameters? = nil,
                                                             encoder: ParameterEncoder,
                                                             headers: HTTPHeaders? = nil) -> Promise<Response> {
        let request = gateway.request(baseUrl + url, method: method, parameters: parameters, encoder: encoder, headers: headers)
        return request.toPromise()
    }

    func request<Response: Decodable>(_ url: String,
                                      method: HTTPMethod,
                                      parameters: Parameters? = nil,
                                      encoding: ParameterEncoding,
                                      headers: HTTPHeaders? = nil) -> Promise<Response> {
        let request = gateway.request(baseUrl + url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        return request.toPromise()
    }

    func request(_ url: String,
                 method: HTTPMethod,
                 parameters: Parameters? = nil,
                 encoding: ParameterEncoding,
                 headers: HTTPHeaders? = nil) -> DataRequest {
        let request = gateway.request(baseUrl + url, method: method, parameters: parameters, encoding: encoding, headers: headers)
        return request
    }
}
