//
//  RequestConvertible.swift
//  AlabsNetworkExample
//
//  Created by Nurbek Ismagulov on 10/2/20.
//  Copyright © 2020 Azimut Labs. All rights reserved.
//

import Alamofire

public enum ParametersInjectionPolicy {
    case include
    case exclude
}

public struct AlamofireNetworkRequest: URLRequestConvertible {
    
    var baseUrl: String
    var path: String?
    var method: HTTPMethod = .get
    var parameters: Parameters?
    var headers: HTTPHeaders?
    
    var injectedParameters: NetworkParametersStorable?
    var parametersInjectionPolicy: ParametersInjectionPolicy
    let initialParametersInjectionPolicy: ParametersInjectionPolicy
    
    private var url: URLConvertible {
        baseUrl + (path ?? "")
    }
    
    private var encoding: ParameterEncoding {
        switch method {
        case .get:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    public init(baseUrl: String, injectedParameters: NetworkParametersStorable?, parametersInjectionPolicy: ParametersInjectionPolicy) {
        self.baseUrl = baseUrl
        self.injectedParameters = injectedParameters
        self.parametersInjectionPolicy = parametersInjectionPolicy
        self.initialParametersInjectionPolicy = parametersInjectionPolicy
    }
    
    public mutating func reset() {
        path = nil
        method = .get
        headers = nil
        parameters = nil
        parametersInjectionPolicy = initialParametersInjectionPolicy
    }
    
    public func asURLRequest() throws -> URLRequest {
        let request = try URLRequest(url: url, method: method, headers: headers)
        var params = parameters
        if let defaultParams = injectedParameters?.parameters, parametersInjectionPolicy == .include {
            params = defaultParams.merging(parameters ?? [:]) { (_, new) in new }
        }
        return try encoding.encode(request, with: params)
    }
}
