//
//  APIClient.swift
//  FDNetworkLayer
//
//  Created by iBol on 4/25/21.
//

import Foundation
import FDUtilities

public protocol APIClient {
	var session: URLSession { get }
	func fetch<T: Codable>(with request: URLRequest, completion: @escaping ResultClosure<T>)
}

public extension APIClient {
	
	typealias JSONTaskCompletionHandler = (Codable?, NetworkError?) -> Void
	
	func fetch<T: Codable>(with request: URLRequest, completion: @escaping ResultClosure<T>) {
		#if DEBUG
        print("URL Request description: \(request.curlString) \n")
		#endif
		decodingTask(with: request, decodingType: T.self) { (json , error) in
			self.resultHandler(json, error, T.self, completion: completion)
		}
	}
	
	func upload<T: Codable>(with request: URLRequest, imageData: Data, completion: @escaping ResultClosure<T>) {
		uploadingTask(with: request, imageData: imageData, decodingType: T.self) { (json, error) in
			self.resultHandler(json, error, T.self, completion: completion)
		}
	}
	
	private func decodingTask<T: Codable>(with request: URLRequest, decodingType: T.Type, completionHandler completion: @escaping JSONTaskCompletionHandler) {
		session.dataTask(with: request) { (data, response, error) in
			DispatchQueue.main.async {
				self.mainDecoding(data, response, decodingType, request, completion: completion)
			}
		}.resume()
	}
	
	private func uploadingTask<T: Codable>(with request: URLRequest, imageData: Data, decodingType: T.Type, completionHandler completion: @escaping JSONTaskCompletionHandler) {
		session.uploadTask(with: request, from: imageData) { (data, response, error) in
			DispatchQueue.main.async {
				self.mainDecoding(data, response, decodingType, request, completion: completion)
			}
		}.resume()
	}
	
}

extension APIClient {
	
	private func mainDecoding<T: Codable>(_ data: Data?, _ response: URLResponse?, _ decodingType: T.Type, _ request: URLRequest, completion: @escaping JSONTaskCompletionHandler) {
		guard let httpResponse = response as? HTTPURLResponse else {
			completion(nil, NetworkError(.requestFailed))
			return
		}
		
		guard httpResponse.statusCode == 200 || httpResponse.statusCode == 201 else {
			completion(nil, NetworkError(.responseUnsuccessful))
			return
		}
		
		guard let data = data else {
			completion(nil, NetworkError(.invalidData))
			return
		}

		serializeResponse(from: data, with: decodingType, and: completion, request: request)
	}
	
	private func resultHandler<T: Codable>(_ json: Codable?, _ error: NetworkError?, _ decodingType: T.Type, completion: @escaping ResultClosure<T>) {
		DispatchQueue.main.async {
			print("JSON:", json)
			guard let json = json else {
				if let error = error {
					completion(Result.failure(error))
				} else {
					completion(Result.failure(NetworkError(.invalidData)))
				}
				return
			}
			
			if let value = json as? T {
				completion(.success(value))
			} else if let value = json as? BaseResponse<T> {
				completion(.success(value.data))
			} else {
				completion(.failure(NetworkError(.jsonParsingFailure)))
			}
		}
	}
	
	private func serializeResponse<T: Codable>(from data: Data, with decodingType: T.Type, and completion: @escaping JSONTaskCompletionHandler, request: URLRequest) {
		do {
			guard let baseDictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
				let error = NetworkError(.invalidData, statusCode: -1, data: nil)
				completion(nil, error)
				return
			}
			
			let someDecodedData = try? JSONDecoder().decode(T.self, from: data)
			print(someDecodedData)
			print("description: ", request.curlString)
			if let errorCode = baseDictionary["error_code"] as? Int, errorCode == 401 {
				print("error", request.description)
				Thread.onMainThread {
					// MARK: TODO
//                        UIApplication.shared.keyWindow?.rootViewController?.showError(with: baseDictionary["message"] as? String)
//                        TechnicalMetrics.sendBackendError(endpoint: "\(request.httpMethod ?? "GET") \(request.description)", errorCode: errorCode)
//                        MainManager.shared.clearAllCacheAndRestart()
				}
				return
			}
			
			print("–----------BASE DICTIONARY DATA BEGIN----------")
			print(baseDictionary)
			if let baseDictionaryData = baseDictionary["data"],
				let baseDictionaryMessage = baseDictionary["message"] as? String,
				let errorCode = baseDictionary["error_code"] as? Int,
				let status = baseDictionary["status"] as? String {
				if baseDictionaryData is NSNull && errorCode != 0 {
					completion(nil, NetworkError(.gatewayError(error: baseDictionaryMessage)))
					// MARK: TODO
//                        TechnicalMetrics.sendBackendError(endpoint: "\(request.httpMethod ?? "GET") \(request.description)", errorCode: errorCode)
					return
				} else if !(baseDictionaryData is NSNull) && errorCode != 0 {
					completion(nil, NetworkError(.gatewayError(error: baseDictionaryMessage), statusCode: errorCode, data: baseDictionaryData as? [String : Any]))
					// MARK: - TODO
//                        TechnicalMetrics.sendBackendError(endpoint: "\(request.httpMethod ?? "GET") \(request.description)", errorCode: errorCode)
					return
				} else if baseDictionaryData is NSNull && errorCode == 0 {
					let baseResponse = BaseResponse<T>(errorCode: errorCode, message: baseDictionaryMessage, status: status, data: nil)
					completion(baseResponse, nil)
					return
				}
			}
			print("–----------BASE DICTIONARY DATA END----------")
			
			let baseResponse = try JSONDecoder().decode(BaseResponse<T>.self, from: data)
			let baseResponseData = baseResponse.data
			completion(baseResponseData, nil)
		} catch let DecodingError.typeMismatch(_, context)  {
			let errorFieldPath = "path: \(context.codingPath.map({ $0.stringValue }).reduce("", { $0 + " -> " + $1 }))"
			print("PATH: ", errorFieldPath)
			// MARK: - TODO
//            TechnicalMetrics.sendSerializeError(endpoint: "\(request.httpMethod ?? "GET") \(request.description)", errorPath: errorFieldPath)
			completion(nil, NetworkError(.jsonConversionFailure(error: "Данные не удалось прочитать, так как они имеют неверный формат"), statusCode: -1))
		} catch {
			completion(nil, NetworkError(.jsonConversionFailure(error: error.localizedDescription), statusCode: -1))
		}
	}
	
}
