//
//  StockProvider.swift
//  FDNetworkLayer
//
//  Created by iBol on 6/8/21.
//

import Alamofire
import Promises

public protocol StockProvider {
	func getStockFood(_ request: StockModels.GetStockFood.Request) -> Promise<StockModels.GetStockFood.Response>
	func createStockOrder(_ request: StockModels.CreateStockOrder.Request) -> Promise<StockModels.CreateStockOrder.Response>
}

extension Provider: StockProvider {
	public func getStockFood(_ request: StockModels.GetStockFood.Request) -> Promise<StockModels.GetStockFood.Response> {
		rest.get("/api/v1/core/exchange/", parameters: request)
	}
	 
	public func createStockOrder(_ request: StockModels.CreateStockOrder.Request) -> Promise<StockModels.CreateStockOrder.Response> {
		rest.post("/api/v1/orders/create/", parameters: request)
	}
}
