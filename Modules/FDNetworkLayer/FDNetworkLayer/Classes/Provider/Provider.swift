//
//  Provider.swift
//  FDNetworkLayer
//
//  Created by Ramazan Kazybek on 5/13/21.
//

public class Provider {
	public let rest: AlamofireNetwork<ProviderError>
	public let basicAuth: String
	public let builder: NetworkRequestBuilder

	public init(rest: AlamofireNetwork<ProviderError>, basicAuth: String, builder: NetworkRequestBuilder) {
		self.rest = rest
		self.basicAuth = basicAuth
		self.builder = builder
	}
}

public class ProviderError: AlamofireNetworkError {
	enum CodingKeys: String, CodingKey {
		case code
		case message
	}
	
	public required init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		let message = try container.decode(String.self, forKey: .message)
		super.init(message: message)
	}
}
