//
//  AuthStateObserver.swift
//  Alamofire
//
//  Created by Ramazan Kazybek on 5/13/21.
//

import FDFoundation
import FDUIKit

public final class AuthStateObserver: NetworkAuthStateObservable {
	private let userSession: UserSession
//	private weak var userRepository: UserRepository?
	private weak var coordinator: BaseCoordinator?

	public init(userSession: UserSession) {
		self.userSession = userSession
	}

	public func tokenDidExpire() {
		forceLogout()
	}

	@objc
	public func forceLogout() {
		configureUserSession()
		configureApplicationCoordinator()
//		configureUserRepository()
	}

	public func setCoordinator(_ coordinator: BaseCoordinator?) {
		self.coordinator = coordinator
	}
	
//	public func setUserRepository(_ repository: UserRepository?) {
//		self.userRepository = repository
//	}

	private func configureUserSession() {
		userSession.finish()
	}
	
//	private func configureUserRepository() {
//		userRepository?.clearRepository()
//	}

	private func configureApplicationCoordinator() {
		coordinator?.clearChildCoordinators()
		coordinator?.router.dismissModule()
		coordinator?.start()
	}
}
