//
//  AuthStateObserverKit.swift
//  FDNetworkLayer
//
//  Created by Ramazan Kazybek on 5/13/21.
//

import EasyDi
import FDFoundation

final public class AuthStateObserverKit: Assembly {
	private let userSessionKit: UserSessionKit = DIContext.defaultInstance.assembly()
	
	public var observer: AuthStateObserver {
		return define(scope: Scope.lazySingleton, init: AuthStateObserver(userSession: self.userSessionKit.service))
	}
}
