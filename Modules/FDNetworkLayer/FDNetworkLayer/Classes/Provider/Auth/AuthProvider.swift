//
//  AuthProvider.swift
//  FDNetworkLayer
//
//  Created by Ramazan Kazybek on 5/13/21.
//

import Alamofire
import Promises

public protocol AuthProvider {
	func validatePhone(_ request: MainModels.ValidatePhone.Request) -> Promise<ValidatePhoneResponseModel>
	func register(_ request: AuthModels.Register.Request) -> Promise<AuthModels.Register.Response>
	func auth(_ request: AuthModels.Auth.Request) -> Promise<NetworkAuthCredentials>
	func requestCode(_ request: AuthModels.SendOtp.Request) -> Promise<AuthModels.SendOtp.Response>
	func verifyOtp(_ request: AuthModels.VerifyOtp.Request) -> Promise<NetworkAuthCredentials>
}

extension Provider: AuthProvider {
	public func validatePhone(_ request: MainModels.ValidatePhone.Request) -> Promise<ValidatePhoneResponseModel> {
		rest.put("/api/v1/auth/validate_phone/", parameters: request)
	}
	
	public func register(_ request: AuthModels.Register.Request) -> Promise<AuthModels.Register.Response> {
		rest.post("/api/v1/registration/", parameters: request)
	}
	
	public func auth(_ request: AuthModels.Auth.Request) -> Promise<NetworkAuthCredentials> {
		rest.post("/api/v1/token/", parameters: request)
	}
	
	public func requestCode(_ request: AuthModels.SendOtp.Request) -> Promise<AuthModels.SendOtp.Response> {
		rest.post("/api/v1/auth/send_otp/", parameters: request)
	}
	
	public func verifyOtp(_ request: AuthModels.VerifyOtp.Request) -> Promise<NetworkAuthCredentials> {
		rest.post("/api/v1/auth/verify_otp/", parameters: request)
	}
}
