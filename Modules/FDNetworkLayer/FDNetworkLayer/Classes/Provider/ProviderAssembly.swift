//
//  ProviderAssembly.swift
//  FDNetworkLayer
//
//  Created by Ramazan Kazybek on 5/13/21.
//

import EasyDi

final public class ProviderAssembly: Assembly {
	private lazy var network: AlamofireNetworkAssembly = DIContext.defaultInstance.assembly()
	
	public var service: Provider {
		return define(scope: Scope.lazySingleton, init: Provider(rest: self.network.service,
																 basicAuth: "",
																 builder: NetworkRequestBuilder(with: "https://dictionary.skyeng.ru")))
	}
}
