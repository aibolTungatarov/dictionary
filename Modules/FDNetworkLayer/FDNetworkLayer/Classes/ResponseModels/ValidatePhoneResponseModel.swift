//
//  ValidatePhoneResponseModel.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/7/21.
//

import Foundation

public struct ValidatePhoneResponseModel: Decodable {
	public let exists: Bool?
}
