//
//  NetworkRequest.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/7/21.
//

import Foundation
import Alamofire

public protocol INetworkRequest {
	associatedtype RequestModel: Encodable
	associatedtype ResponseModel: Decodable
	
	var path: String { get }
	var method: NetworkMethod { get }
	var body: RequestModel { get }
	var queryItems: [URLQueryItem]? { get }
	var fullPath: String? { get }
	var parameters: Parameters? { get }
}

public extension INetworkRequest {
	var queryItems: [URLQueryItem]? {
		return nil
	}
	
	public var fullPath: String? {
		guard var urlComponents = URLComponents(string: path) else { return nil }
		urlComponents.queryItems = queryItems
		return urlComponents.url?.absoluteString
	}
}
