//
//  StockModels.swift
//  FDNetworkLayer
//
//  Created by iBol on 6/8/21.
//

import Foundation

public enum StockModels {
	public enum GetStockFood {
		public struct Request: Codable {
			public let address: String?
			public let name: String?
			public let priceMax: Int?
			public let priceMin: Int?
			public let weight: Float?
			
			public init(address: String?, name: String?, priceMax: Int?, priceMin: Int?, weight: Float?) {
				self.address = address
				self.name = name
				self.priceMax = priceMax
				self.priceMin = priceMin
				self.weight = weight
			}
			
			enum CodingKeys: String, CodingKey {
				case address
				case name
				case priceMax = "price_max"
				case priceMin = "price_min"
				case weight
			}
		}
		
		public struct Response: Decodable {
			public let results: [StockFood]?
		}
	}
	
	public enum CreateStockOrder {
		public struct Request: Codable {
			public let cookId: Int?
			public let productId: Int?
			
			enum CodingKeys: String, CodingKey {
				case cookId = "cook"
				case productId = "product"
			}
			
			public init(cookId: Int?, productId: Int?) {
				self.cookId = cookId
				self.productId = productId
			}
		}
		
		public struct Response: Decodable {}
	}
}

public struct StockFood: Decodable {
	public let id: Int?
	public let name: String?
	public let description: String?
	public let price: String?
	public let photo: String?
	public let cook: StockCook?
}

public struct StockCook: Decodable {
	public let id: Int?
	public let lastName: String?
	public let firstName: String?
	public let avatar: String?
	
	enum CodingKeys: String, CodingKey {
		case id
		case lastName = "last_name"
		case firstName = "first_name"
		case avatar
	}
}
