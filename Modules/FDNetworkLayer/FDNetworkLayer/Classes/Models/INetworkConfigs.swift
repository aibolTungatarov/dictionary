//
//  INetworkConfigs.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/7/21.
//

import Foundation

public protocol INetworkConfigs {
	var baseURL: String { get }
	var port: Int? { get }
}
