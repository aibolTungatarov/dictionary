//
//  NetworkMethod.swift
//  FDNetworkLayer
//
//  Created by iBol on 5/7/21.
//

import Foundation

public enum NetworkMethod: String {
	case GET
	case POST
	case PUT
	case DELETE
}
