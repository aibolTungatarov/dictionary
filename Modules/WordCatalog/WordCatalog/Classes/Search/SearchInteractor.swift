//
//  SearchInteractor.swift
//  Main
//
//  Created by iBol on 8/8/21.
//

import UIKit
import WordCatalogKit

protocol ISearchInteractor: AnyObject {
	func searchTranslation(with text: String, loadMore: Bool, completion: @escaping ([SearchModel.Response?]) -> ())
	func didSelectItem(id: Int)
}

final class SearchInteractor: ISearchInteractor {
	// MARK: - Deps
	weak var controller: ISearchView?
	weak var coordinator: ISearchCoordinatorOutput?
	
	public var translations = [SearchModel.Response]()
	private let service: SearchService
	private var pendingRequestWorkItem: DispatchWorkItem?
	private var currentPage = 1
	private var isFetchInProgress = false
	private var total = 0
	
	init(service: SearchService) {
		self.service = service
	}
	
	func searchTranslation(with text: String, loadMore: Bool = false, completion: @escaping ([SearchModel.Response?]) -> ()) {
		pendingRequestWorkItem?.cancel()
		
		let requestWorkItem = DispatchWorkItem { [weak self] in
			self?.loadSearchResults(with: text, completion: completion, loadMore: loadMore)
		}
		
		pendingRequestWorkItem = requestWorkItem
		DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500),
									  execute: requestWorkItem)
	}
	
	private func loadSearchResults(with text: String, completion: @escaping ([SearchModel.Response?]) -> (), loadMore: Bool = false) {
		guard !isFetchInProgress else {
			return
		}
		isFetchInProgress = true
		currentPage = loadMore ? currentPage + 1 : 1
		
		let requestModel = SearchModel.Request(search: text, page: String(currentPage), pageSize: "10")
		let request = SearchRequest(requestModel: requestModel)
		service.searchTranslation(request)
			.then { [weak self] result in
				DispatchQueue.main.async {
					self?.isFetchInProgress = false
					self?.total = result.count
					if loadMore {
						self?.translations.append(contentsOf: result)
					} else {
						self?.translations = result
					}
					completion(self?.translations ?? [])
				}
			}
			.catch { [weak self] error in
				print("AIBOL: ", error)
				self?.isFetchInProgress = false
			}
	}
	
	func didSelectItem(id: Int) {
		coordinator?.navigateToDetails(id: id)
	}
	
	private func calculateIndexPathsToReload(from newTranslations: [SearchModel.Response]) -> [IndexPath] {
	  let startIndex = translations.count - newTranslations.count
	  let endIndex = startIndex + newTranslations.count
	  return (startIndex..<endIndex).map { IndexPath(row: 0, section: $0) }
	}
}
