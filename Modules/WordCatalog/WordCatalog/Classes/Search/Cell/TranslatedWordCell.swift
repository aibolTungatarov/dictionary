//
//  TranslatedWordCell.swift
//  WordCatalog
//
//  Created by iBol on 8/8/21.
//

import FDUIKit
import WordCatalogKit

protocol ConfigurableTableViewCell: UITableViewCell {
	func configure(with viewModel: AnyObject)
}

final class TranslatedWordCell: UITableViewCell {
	private var viewModel: Meaning?
	private let customSelectedBackgroundView = UIView()
	
	private let wordImageView: UIImageView = {
		let view = UIImageView()
		view.clipsToBounds = true
		view.layer.borderWidth = 0.5
		view.layer.borderColor = UIColor.appGray.cgColor
		return view
	}()
	
	private let wordTitleLabel: UILabel =
		UILabelFactory(text: "")
		.font(FontFamily.Roboto.regular.font(size: 18))
		.text(color: .appBlack)
		.numberOf(lines: 0)
		.build()
	
	private let translatedWordLabel: UILabel =
		UILabelFactory(text: "")
		.font(FontFamily.Roboto.regular.font(size: 14))
		.text(color: .appGray)
		.numberOf(lines: 0)
		.build()
			
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		selectedBackgroundView = customSelectedBackgroundView
		setupViews()
		setupConstraints()
	}
	
	private let containerView = UIView()
	
	required init?(coder: NSCoder) {
		nil
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		containerView.corner(radius: 18)
		wordImageView.corner(radius: 7)
	}
	
	private func setupViews() {
		addSubview(containerView)
		containerView.backgroundColor = .wordBackgroundGray
		[wordImageView, wordTitleLabel, translatedWordLabel].forEach {
			containerView.addSubview($0)
		}
	}
	
	private func setupConstraints() {
		containerView.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(10)
			make.right.equalToSuperview().offset(-10)
			make.top.equalToSuperview().offset(5)
			make.bottom.equalToSuperview().offset(-5)
		}
		
		wordImageView.snp.makeConstraints { make in
			make.height.equalTo(40)
			make.width.equalTo(50)
			make.left.equalToSuperview().offset(10)
			make.top.equalToSuperview().offset(8)
			make.bottom.equalToSuperview().offset(-8)
		}
		
		wordTitleLabel.snp.makeConstraints { make in
			make.top.equalTo(wordImageView)
			make.left.equalTo(wordImageView.snp.right).offset(10)
			make.right.equalToSuperview().offset(-10)
		}
		
		translatedWordLabel.snp.makeConstraints { make in
			make.top.equalTo(wordTitleLabel.snp.bottom).offset(2)
			make.left.right.equalTo(wordTitleLabel)
			make.bottom.equalToSuperview().offset(-8)
		}
	}
}

extension TranslatedWordCell {
	func configure(with viewModel: Meaning, title: String) {
		self.viewModel = viewModel
		wordTitleLabel.text = title
		translatedWordLabel.text = viewModel.translation?.text
		guard let imageUrl = URL(string: "https:" + (viewModel.imageUrl ?? "")) else { return }
		wordImageView.kf.setImage(with: imageUrl)
	}
}

protocol ITableViewCellViewModel: AnyObject {
	associatedtype Cell: ConfigurableTableViewCell
}
