//
//  SearchCoordinator.swift
//  Main
//
//  Created by iBol on 8/8/21.
//

import FDUIKit
import EasyDi

public typealias ISearchCoordinatorOutput = (ISearchCoordinator & ExitCoordinator)

public protocol ISearchCoordinator: AnyObject {
	func navigateToDetails(id: Int)
}

public final class SearchCoordinator: BaseCoordinator, ISearchCoordinatorOutput {
	// MARK: - ExitCoordinator
	
	public var exitCompletion: Callback?
	
	// MARK: - Lifecycle
	
	public override init(router: Router) {
		super.init(router: router)
	}
	
	public override func start() {
		navigateToMain()
	}
	
	private func navigateToMain() {
		let assembly: SearchAssembly = DIContext.defaultInstance.assembly()
		let controller = assembly.makeModule(with: self)
		router.push(controller, animated: true, hideBottomBarWhenPushed: false, completion: nil)
	}
	
	public func navigateToDetails(id: Int) {
		TranslationDetailsCoordinator(router: router).start(with: id)
	}
}
