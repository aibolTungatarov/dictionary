//
//  SearchViewController.swift
//  Main
//
//  Created by iBol on 8/8/21.
//

import FDUIKit
import SnapKit

protocol ISearchView: AnyObject {}

final class SearchViewController: UIViewController {
	// MARK: - Deps
	private let interactor: ISearchInteractor
	
	// MARK: - Views
	private lazy var rootView: SearchView = {
		let view = SearchView()
		view.delegate = self
		return view
	}()
	
	private lazy var searchController: UISearchController = {
		let controller = UISearchController(searchResultsController: nil)
		controller.searchResultsUpdater = self
		controller.obscuresBackgroundDuringPresentation = false
		controller.searchBar.placeholder = "Search new words"
		definesPresentationContext = true
		return controller
	}()
	
	// MARK: - Lifecycle
	init(interactor: ISearchInteractor) {
		self.interactor = interactor
		
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.addSubview(rootView)
		rootView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		title = "Word Catalog"
		navigationItem.searchController = searchController
		navigationItem.setHidesBackButton(true, animated: true)
		navigationItem.hidesSearchBarWhenScrolling = false
	}
	
	private func loadSearchResults(loadMore: Bool = false, completion: @escaping(() -> ())) {
		guard let searchText = searchController.searchBar.text else { return }
		interactor.searchTranslation(with: searchText, loadMore: loadMore) { [weak self] result in
			self?.rootView.items = result
			completion()
		}
	}
}

// MARK: - ISearchView
extension SearchViewController: ISearchView {}

// MARK: - UISearchResultsUpdating
extension SearchViewController: UISearchResultsUpdating {
	func updateSearchResults(for searchController: UISearchController) {
		loadSearchResults {}
	}
}

extension SearchViewController: SearchViewDelegate {
	func didSelectItem(id: Int) {
		interactor.didSelectItem(id: id)
	}
	
	func loadMore() {
		loadSearchResults(loadMore: true) { [weak self] in
			self?.rootView.stopLoading()
		}
	}
	
	func refresh() {
		loadSearchResults { [weak self] in
			self?.rootView.stopRefreshing()
		}
	}
}
