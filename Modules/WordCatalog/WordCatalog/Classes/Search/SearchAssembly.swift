//
//  SearchAssembly.swift
//  Main
//
//  Created by iBol on 8/8/21.
//

import EasyDi
import WordCatalogKit

final class SearchAssembly: Assembly {
	private let searchKitAssembly: SearchKitAssembly = DIContext.defaultInstance.assembly()
	
	// MARK: - ViewController
	func makeModule(with coordinator: ISearchCoordinatorOutput) -> UIViewController {
		let interactor = makeInteractor()
		let controller = SearchViewController(interactor: interactor)
		
		return define(init: controller) {
			interactor.controller = $0
			interactor.coordinator = coordinator
			return $0
		}
	}
	
	// MARK: - Interactor
	private func makeInteractor() -> SearchInteractor {
		return SearchInteractor(service: searchKitAssembly.service)
	}
}
