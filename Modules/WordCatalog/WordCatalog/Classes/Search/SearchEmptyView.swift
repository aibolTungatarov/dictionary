//
//  SearchEmptyView.swift
//  WordCatalog
//
//  Created by iBol on 8/9/21.
//

import FDUIKit

class SearchEmptyView: UIView {
	
	private let soundIcon = UIImage.makeFromBundle(imageName: "magnifier")
	private lazy var imageView = UIImageView(image: soundIcon ?? .init())
	private let emptyLabel: UILabel =
		UILabelFactory(text: "Search for words and word sets")
		.font(FontFamily.Roboto.regular.font(size: 20))
		.text(color: .appGray)
		.text(alignment: .center)
		.numberOf(lines: 0)
		.build()
	
	// MARK: - Inits
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = .white
		addSubviews()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}

extension SearchEmptyView: Decoratable {
	func addSubviews() {
		let bundle = Bundle(for: Self.self)
		addSubview(imageView)
		addSubview(emptyLabel)
	}
	
	func setupLayout() {
		emptyLabel.snp.makeConstraints { make in
			make.top.equalTo(imageView.snp.bottom).offset(30)
			make.center.equalToSuperview()
			make.left.equalToSuperview().offset(15)
			make.right.equalToSuperview().offset(-15)
		}
		
		imageView.snp.makeConstraints { make in
			make.height.width.equalTo(100)
			make.centerX.equalToSuperview()
		}
	}
}
