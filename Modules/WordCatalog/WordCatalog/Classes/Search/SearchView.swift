//
//  SearchView.swift
//  Main
//
//  Created by iBol on 8/8/21.
//

import FDUIKit
import WordCatalogKit

protocol SearchViewDelegate: AnyObject {
	func didSelectItem(id: Int)
	func loadMore()
	func refresh()
}

class SearchView: UIView {
	public weak var delegate: SearchViewDelegate?
	public var items = [SearchModel.Response?]() {
		didSet {
			tableView.backgroundView?.isHidden = !items.isEmpty
			tableView.reloadData()
		}
	}
	
	// MARK: - Views
	private lazy var tableView: TableView = {
		let view = TableView(frame: .zero, style: .grouped)
		view.backgroundView = SearchEmptyView()
		view.delegate = self
		view.dataSource = self
		view.loadMoreRefreshManagerDelegate = self
		view.refreshManagerDelegate = self
		view.register(TranslatedWordCell.self)
		view.separatorStyle = .none
		view.alwaysBounceHorizontal = false
		view.backgroundColor = .white
		return view
	}()
	
	// MARK: - Inits
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = .white
		addSubviews()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	public func stopRefreshing() {
		tableView.endRefreshingTable()
	}
	
	public func stopLoading() {
		tableView.endRefreshingTable(loadMoreStatus: false)
	}
}

extension SearchView: Decoratable {
	func addSubviews() {
		addSubview(tableView)
	}
	
	func setupLayout() {
		tableView.snp.makeConstraints { make in
			make.left.right.equalToSuperview()
			make.top.bottom.equalTo(safeAreaLayoutGuide)
		}
	}
}

extension SearchView: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let item = items[indexPath.section]?.meanings?[indexPath.row]
		guard let id = item?.id else { return }
		delegate?.didSelectItem(id: id)
	}
	
	func scrollViewDidScroll(_ scrollView: UIScrollView) {
		tableView.loadMoreManager.didScroll(scrollView: scrollView)
	}
}

extension SearchView: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return items[section]?.meanings?.count ?? 0
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return items.count
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return items[section]?.text
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell: TranslatedWordCell = tableView.dequeueReusableCell(for: indexPath)
		guard let item = items[indexPath.section]?.meanings?[indexPath.row] else { return UITableViewCell() }
		cell.configure(with: item, title: items[indexPath.section]?.text ?? "")
		return cell
	}
}

extension SearchView: LoadMoreRefreshManagerDelegate {
	func loadMore() {
		delegate?.loadMore()
	}
}

extension SearchView: RefreshManagerDelegate {
	func refresh() {
		delegate?.refresh()
	}
}
