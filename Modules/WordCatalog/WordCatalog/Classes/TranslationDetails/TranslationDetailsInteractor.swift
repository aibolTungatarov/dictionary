//
//  TranslationDetailsInteractor.swift
//  WordCatalog
//
//  Created by iBol on 8/9/21.
//

import UIKit
import WordCatalogKit

protocol ITranslationDetailsInteractor: AnyObject {
	func getDetails(completion: @escaping (DetailsModel.Response?) -> ())
}

final class TranslationDetailsInteractor: ITranslationDetailsInteractor {
	// MARK: - Deps
	weak var controller: ITranslationDetailsView?
	weak var coordinator: ITranslationDetailsCoordinatorOutput?
	private let service: SearchService
	private let id: Int
	
	init(service: SearchService, id: Int) {
		self.service = service
		self.id = id
	}
	
	func getDetails(completion: @escaping (DetailsModel.Response?) -> ()) {
		let requestModel = DetailsModel.Request(ids: String(id))
		let request = DetailsRequest(requestModel: requestModel)
		service.getDetails(request)
			.then { [weak self] result in
				completion(result.first)
			}
			.catch { [weak self] error in
				print("AIBOL: ", error)
			}
	}
}
