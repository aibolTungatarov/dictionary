//
//  TranslationDetailsView.swift
//  WordCatalog
//
//  Created by iBol on 8/9/21.
//

import FDUIKit
import WordCatalogKit
import AVFoundation
import FDUtilities

class TranslationDetailsView: UIView {
	
	private var model: DetailsModel.Response?
	private var player: AVPlayer?
	private var playerItem: AVPlayerItem?
	
	// MARK: - Views
	private let imageView: UIImageView = {
		let view = UIImageView()
		view.backgroundColor = .imageBackgroundGray
		view.addShadow(ofColor: .imageBackgroundGray, radius: 5, offset: .init(width: 10, height: 10), opacity: 0.8)
		view.contentMode = .scaleAspectFill
		view.clipsToBounds = true
		return view
	}()
	
	private let imageOverlayView: UIView = {
		let view = UIView()
		view.backgroundColor = .appLightBlue
		view.clipsToBounds = true
		return view
	}()
	
	private let wordTitleLabel: UILabel =
		UILabelFactory(text: "")
		.font(FontFamily.Roboto.bold.font(size: 27))
		.text(color: .white)
		.build()
	
	private let translationLabel: UILabel =
		UILabelFactory(text: "")
		.font(FontFamily.Roboto.regular.font(size: 18))
		.text(color: .white)
		.build()
	
	private let soundButton: UIButton = {
		let button = UIButton(type: .custom)
		let soundIcon = UIImage.makeFromBundle(imageName: "sound")?.withRenderingMode(.alwaysTemplate)
		let image = soundIcon?.maskWithColor(color: .white)
		button.setImage(image, for: .normal)
		return button
	}()
	
	private let transcriptionLabel: UILabel =
		UILabelFactory(text: "")
		.font(FontFamily.Roboto.regular.font(size: 18))
		.text(color: .appBlack)
		.build()
	
	private let definitionLabel: UILabel =
		UILabelFactory(text: "")
		.font(FontFamily.Roboto.regular.font(size: 18))
		.text(color: .appBlack)
		.build()
	
	private lazy var examplesTableView: UITableView = {
		let view = SelfSizingTableView()
		view.delegate = self
		view.dataSource = self
		view.register(WordExampleCell.self)
		view.separatorStyle = .none
		view.alwaysBounceHorizontal = false
		view.backgroundColor = .white
		view.estimatedRowHeight = 100
		return view
	}()
	
	private let scrollView: UIScrollView = {
		let view = UIScrollView()
		view.alwaysBounceHorizontal = false
		view.alwaysBounceVertical = true
		return view
	}()
	
	private let contentView = UIView()
	
	// MARK: - Inits
	override init(frame: CGRect) {
		super.init(frame: frame)
		
		backgroundColor = .white
		soundButton.addTarget(self, action: #selector(playWordSound), for: .touchUpInside)
		addSubviews()
		setupLayout()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		imageView.corner(radius: 10)
	}
	
	func setupAudioPlayer(with soundUrlPath: String?) {
		guard let soundUrl = URL(string: "https:" + (soundUrlPath ?? "")) else { return }
		let playerItem = AVPlayerItem(url: soundUrl)
		player = AVPlayer(playerItem: playerItem)
		let playerLayer = AVPlayerLayer(player: player)
		layer.addSublayer(playerLayer)
		guard let player = player else { return }
		player.play()
	}
	
	func configure(with model: DetailsModel.Response?) {
		guard let model = model else { return }
		self.model = model
		wordTitleLabel.text = model.text
		translationLabel.text = model.translation?.text
		transcriptionLabel.text = "/" + (model.transcription ?? "") + "/"
		definitionLabel.text = model.definition?.text
		setupAudioPlayer(with: model.soundUrl)
		examplesTableView.reloadData()
		guard let imageUrlPathComponents = model.images?.first?.url?.split(separator: "?"),
			  let imageUrl = URL(string: "https:" + imageUrlPathComponents[0] + "?w=500&h=375")
			  else { return }
		imageView.kf.setImage(with: imageUrl)
	}
	
	@objc func playWordSound() {
		print("AIBOL")
		player?.pause()
		player?.seek(to: CMTime.zero)
		player?.play()
	}
}

extension TranslationDetailsView: Decoratable {
	func addSubviews() {
		addSubview(scrollView)
		scrollView.addSubview(contentView)
		[imageView, transcriptionLabel, definitionLabel, examplesTableView].forEach {
			contentView.addSubview($0)
		}
		imageView.addSubview(imageOverlayView)
		[wordTitleLabel, translationLabel, soundButton].forEach {
			imageOverlayView.addSubview($0)
		}
	}
	
	func setupLayout() {
		scrollView.snp.makeConstraints { make in
			make.left.right.top.bottom.equalTo(safeAreaLayoutGuide)
		}
		
		contentView.snp.makeConstraints { make in
			make.edges.equalToSuperview()
			make.width.equalTo(self)
		}
		
		imageView.snp.makeConstraints { make in
			make.top.equalToSuperview().offset(20)
			make.left.equalToSuperview().offset(15)
			make.right.equalToSuperview().offset(-15)
			make.height.equalTo(imageView.snp.width).multipliedBy(0.75)
		}
		
		imageOverlayView.snp.makeConstraints { make in
			make.bottom.left.right.equalToSuperview()
		}
		
		wordTitleLabel.snp.makeConstraints { make in
			make.top.equalToSuperview().offset(12)
			make.left.equalToSuperview().offset(15)
			make.right.equalTo(soundButton.snp.left).offset(-15)
		}
		
		translationLabel.snp.makeConstraints { make in
			make.top.equalTo(wordTitleLabel.snp.bottom).offset(3)
			make.left.right.equalTo(wordTitleLabel)
			make.bottom.equalToSuperview().offset(-15)
		}
		
		soundButton.snp.makeConstraints { make in
			make.top.equalTo(wordTitleLabel)
			make.right.equalToSuperview().offset(-20)
			make.width.height.equalTo(25)
		}
		
		transcriptionLabel.snp.makeConstraints { make in
			make.top.equalTo(imageView.snp.bottom).offset(20)
			make.left.equalTo(imageView).offset(5)
			make.right.equalTo(imageView).offset(-5)
		}
		
		definitionLabel.snp.makeConstraints { make in
			make.top.equalTo(transcriptionLabel.snp.bottom).offset(7)
			make.left.right.equalTo(transcriptionLabel)
		}
		
		examplesTableView.snp.makeConstraints { make in
			make.top.equalTo(definitionLabel.snp.bottom).offset(15)
			make.left.right.equalToSuperview()
		}
	}
}

extension TranslationDetailsView: UITableViewDelegate {}

extension TranslationDetailsView: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return model?.examples?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let model = model else { return UITableViewCell() }
		let cell: WordExampleCell = tableView.dequeueReusableCell(for: indexPath)
		cell.configure(with: model.examples?[indexPath.row])
		return cell
	}
}
