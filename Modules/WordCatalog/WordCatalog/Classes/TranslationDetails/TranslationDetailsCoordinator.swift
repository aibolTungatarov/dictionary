//
//  TranslationDetailsCoordinator.swift
//  WordCatalog
//
//  Created by iBol on 8/9/21.
//

import FDUIKit
import EasyDi

public typealias ITranslationDetailsCoordinatorOutput = (ITranslationDetailsCoordinator & ExitCoordinator)

public protocol ITranslationDetailsCoordinator: AnyObject {}

public final class TranslationDetailsCoordinator: BaseCoordinator, ITranslationDetailsCoordinatorOutput {
	// MARK: - ExitCoordinator
	
	public var exitCompletion: Callback?
	
	// MARK: - Lifecycle
	
	public override init(router: Router) {
		super.init(router: router)
	}
	
	public func start(with id: Int) {
		navigateToDetails(id: id)
	}
	
	private func navigateToDetails(id: Int) {
		let assembly: TranslationDetailsAssembly = DIContext.defaultInstance.assembly()
		let controller = assembly.makeModule(with: self, id: id)
		router.push(controller, animated: true, hideBottomBarWhenPushed: false, completion: nil)
	}
}
