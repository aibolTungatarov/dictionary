//
//  WordExampleCell.swift
//  WordCatalog
//
//  Created by iBol on 8/9/21.
//

import FDUIKit
import AVFoundation
import WordCatalogKit

final class WordExampleCell: UITableViewCell {
	private var player: AVPlayer?
	
	private let soundButton: UIButton = {
		let button = UIButton(type: .custom)
		let playIcon = UIImage.makeFromBundle(imageName: "play")
		button.setImage(playIcon, for: .normal)
		return button
	}()
	
	private let exampleWordLabel: UILabel =
		UILabelFactory(text: "")
		.font(FontFamily.Roboto.regular.font(size: 18))
		.text(color: .placeholderGray)
		.numberOf(lines: 0)
		.build()
	
	private let containerView = UIView()
			
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		selectionStyle = .none
		setupViews()
		setupConstraints()
	}
	
	required init?(coder: NSCoder) {
		nil
	}
	
	func setupAudioPlayer(with soundUrlPath: String?) {
		guard let soundUrl = URL(string: "https:" + (soundUrlPath ?? "")) else { return }
		let playerItem = AVPlayerItem(url: soundUrl)
		player = AVPlayer(playerItem: playerItem)
		let playerLayer = AVPlayerLayer(player: player)
		layer.addSublayer(playerLayer)
	}
	
	private func setupViews() {
		soundButton.addTarget(self, action: #selector(playExampleSound), for: .touchUpInside)
		contentView.addSubview(containerView)
		containerView.backgroundColor = .white
		containerView.addSubview(soundButton)
		containerView.addSubview(exampleWordLabel)
	}
	
	private func setupConstraints() {
		containerView.snp.makeConstraints { make in
			make.left.equalToSuperview().offset(15)
			make.right.equalToSuperview().offset(-15)
			make.top.equalToSuperview().offset(7)
			make.bottom.equalToSuperview().offset(-7)
		}
		
		soundButton.snp.makeConstraints { make in
			make.left.top.equalToSuperview()
			make.height.width.equalTo(20)
		}
		
		exampleWordLabel.snp.makeConstraints { make in
			make.top.right.bottom.equalToSuperview()
			make.left.equalTo(soundButton.snp.right).offset(10)
		}
	}
	
	@objc func playExampleSound() {
		print("ad")
		player?.pause()
		player?.seek(to: .zero)
		player?.play()
	}
}

extension WordExampleCell {
	func configure(with viewModel: WordExample?) {
		guard let viewModel = viewModel else { return }
		exampleWordLabel.text = viewModel.text
		setupAudioPlayer(with: viewModel.soundUrl)
	}
}
