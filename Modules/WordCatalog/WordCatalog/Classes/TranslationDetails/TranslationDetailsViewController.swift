//
//  TranslationDetailsViewController.swift
//  WordCatalog
//
//  Created by iBol on 8/9/21.
//

import FDUIKit
import SnapKit

protocol ITranslationDetailsView: AnyObject {}

final class TranslationDetailsViewController: UIViewController {
	// MARK: - Deps
	private let interactor: ITranslationDetailsInteractor
	private let rootView = TranslationDetailsView()
	
	// MARK: - Lifecycle
	init(interactor: ITranslationDetailsInteractor) {
		self.interactor = interactor
		
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func loadView() {
		view = rootView
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		interactor.getDetails { [weak self] result in
			self?.rootView.configure(with: result)
		}
	}
}

// MARK: - ISearchView
extension TranslationDetailsViewController: ITranslationDetailsView {}
