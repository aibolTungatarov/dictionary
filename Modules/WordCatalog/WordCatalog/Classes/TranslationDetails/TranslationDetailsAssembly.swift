//
//  TranslationDetailsAssembly.swift
//  WordCatalog
//
//  Created by iBol on 8/9/21.
//

import EasyDi
import WordCatalogKit

final class TranslationDetailsAssembly: Assembly {
	private let searchKitAssembly: SearchKitAssembly = DIContext.defaultInstance.assembly()
	
	// MARK: - ViewController
	func makeModule(with coordinator: ITranslationDetailsCoordinatorOutput, id: Int) -> UIViewController {
		let interactor = makeInteractor(id: id)
		let controller = TranslationDetailsViewController(interactor: interactor)
		
		return define(init: controller) {
			interactor.controller = $0
			interactor.coordinator = coordinator
			return $0
		}
	}
	
	// MARK: - Interactor
	private func makeInteractor(id: Int) -> TranslationDetailsInteractor {
		return TranslationDetailsInteractor(service: searchKitAssembly.service, id: id)
	}
}
