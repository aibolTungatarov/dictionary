Pod::Spec.new do |s|
  s.name             = 'WordCatalog'
  s.version          = '0.1.0'
  s.summary          = 'A short description of WordCatalog.'
  s.homepage         = 'https://github.com/iBol/Main'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'iBol' => 'Aybol.Tungatarov@sberbank.kz' }
  s.source           = { :git => 'https://github.com/iBol/WordCatalog.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'WordCatalog/Classes/**/*'
	s.resource_bundles = { 'WordCatalog-Assets' => ['WordCatalog/Assets/**'] }
	s.dependency 'EasyDi'
	s.dependency 'SnapKit'
	s.dependency 'FDUIKit'
	s.dependency 'WordCatalogKit'
end
