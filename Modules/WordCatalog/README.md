# Main

[![CI Status](https://img.shields.io/travis/iBol/Main.svg?style=flat)](https://travis-ci.org/iBol/Main)
[![Version](https://img.shields.io/cocoapods/v/Main.svg?style=flat)](https://cocoapods.org/pods/Main)
[![License](https://img.shields.io/cocoapods/l/Main.svg?style=flat)](https://cocoapods.org/pods/Main)
[![Platform](https://img.shields.io/cocoapods/p/Main.svg?style=flat)](https://cocoapods.org/pods/Main)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Main is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Main'
```

## Author

iBol, Aybol.Tungatarov@sberbank.kz

## License

Main is available under the MIT license. See the LICENSE file for more info.
