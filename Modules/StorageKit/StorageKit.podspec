Pod::Spec.new do |s|
  s.name             = 'StorageKit'
  s.version          = '0.1.0'
  s.summary          = 'A short description of StorageKit.'
  s.homepage         = 'https://github.com/iBol/StorageKit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'iBol' => 'aibolseed@gmail.com' }
  s.source           = { :git => 'https://github.com/iBol/StorageKit.git', :tag => s.version.to_s }
  s.ios.deployment_target = '12.0'
  s.source_files = 'StorageKit/Classes/**/*'
  s.dependency 'EasyDi'
	s.dependency 'KeyValueStorage'
	s.dependency 'KeychainSwift'
end
