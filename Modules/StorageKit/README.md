# StorageKit

[![CI Status](https://img.shields.io/travis/iBol/StorageKit.svg?style=flat)](https://travis-ci.org/iBol/StorageKit)
[![Version](https://img.shields.io/cocoapods/v/StorageKit.svg?style=flat)](https://cocoapods.org/pods/StorageKit)
[![License](https://img.shields.io/cocoapods/l/StorageKit.svg?style=flat)](https://cocoapods.org/pods/StorageKit)
[![Platform](https://img.shields.io/cocoapods/p/StorageKit.svg?style=flat)](https://cocoapods.org/pods/StorageKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

StorageKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'StorageKit'
```

## Author

iBol, aibolseed@gmail.com

## License

StorageKit is available under the MIT license. See the LICENSE file for more info.
