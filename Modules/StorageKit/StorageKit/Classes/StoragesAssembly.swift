//
//  StoragesAssembly.swift
//  StorageKit
//
//  Created by iBol on 5/3/21.
//

import UIKit
import EasyDi
import KeyValueStorage
import KeychainSwift

public class StoragesAssembly: Assembly {
	public var keyValueStorage: IKeyValueStorage {
		return define(scope: .lazySingleton, init: Defaults())
	}
	
	public var keychainStorage: KeychainSwift {
		return define(scope: .lazySingleton, init: KeychainSwift())
	}
	
//	public var personInfoService: IPersonInfoService {
//		return define(scope: .lazySingleton, init: PersonInfoService(self.keychainStorage))
//	}
}
