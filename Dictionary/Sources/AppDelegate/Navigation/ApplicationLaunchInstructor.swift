//
//  ApplicationLaunchInstructor.swift
//  Dictionary
//
//  Created by iBol on 8/8/21.
//

import FDNetworkLayer
import EasyDi
import FDFoundation

enum ApplicationLaunchInstruction {
	case unauthorized
	case pinCheck
	case profileCreate
}

final class ApplicationLaunchInstructor {
	private let userSessionAssemblyKit: UserSessionKit = DIContext.defaultInstance.assembly()
	
	var flow: ApplicationLaunchInstruction {
		return userSessionAssemblyKit.service.isExists ? (userSessionAssemblyKit.service.needsProfileCreation ? .profileCreate : .pinCheck) : .unauthorized
	}
}

