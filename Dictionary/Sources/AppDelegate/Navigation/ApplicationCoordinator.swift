//
//  ApplicationCoordinator.swift
//  Dictionary
//
//  Created by iBol on 8/8/21.
//

import FDUIKit
import WordCatalog

final class ApplicationCoordinator: BaseCoordinator {
	private let launchInstructor: ApplicationLaunchInstructor

	override init(router: Router) {
		launchInstructor = ApplicationLaunchInstructor()
		super.init(router: router)
	}

	override func start() {
		runMainFlow()
	}

	private func runMainFlow() {
		clearChildCoordinators()
		let coordinator = SearchCoordinator(router: router)
		addDependency(coordinator)
		coordinator.start()
	}
}
