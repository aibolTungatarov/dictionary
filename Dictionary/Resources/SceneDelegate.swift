//
//  SceneDelegate.swift
//  Dictionary
//
//  Created by iBol on 8/8/21.
//

import FDUIKit
import EasyDi
import FDNetworkLayer

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

	var window: UIWindow?
	
	private var applicationCoordinator: ApplicationCoordinator?
	private let authState: AuthStateObserverKit = DIContext.defaultInstance.assembly()
	
	func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
		
		guard let windowScene = (scene as? UIWindowScene) else { return }
		
		let window = UIWindow(windowScene: windowScene)
		let rootController = UINavigationController(rootViewController: UIViewController())
		window.rootViewController = rootController
		self.window = window
		applicationCoordinator = ApplicationCoordinator(router: Router(rootController: rootController))
		authState.observer.setCoordinator(applicationCoordinator)
		applicationCoordinator?.start()
		window.makeKeyAndVisible()
	}
}

